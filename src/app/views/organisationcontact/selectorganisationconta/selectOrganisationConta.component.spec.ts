import { SharedModule } from '../../../shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule, APP_BASE_HREF } from '@angular/common';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { ComponentFixture } from '@angular/core/testing';
import { TestBed, async } from '@angular/core/testing';
import { TerminalClientService } from '../../../framework/services/terminal-client.service';
import { Logger } from '@nsalaun/ng-logger';
import { GrowlModule } from 'primeng/primeng';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { SelectOrganisationContaComponent} from './selectOrganisationConta.component';

describe('Component: SelectOrganisationContaComponent', () => {
    let component: SelectOrganisationContaComponent;
    let fixture: ComponentFixture<SelectOrganisationContaComponent>;
  
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                BrowserModule,
                CommonModule,
                FormsModule,
                GrowlModule,
                HttpClientTestingModule,
                ReactiveFormsModule,
                RouterModule,
                RouterTestingModule,
                SharedModule
            ],
            declarations: [SelectOrganisationContaComponent],
            providers: [
                Logger,
                TerminalClientService,
                {
                    provide: APP_BASE_HREF, useValue: '/'
                }
            ]
        }).compileComponents();
    }));
  
    beforeEach(() => {
        fixture = TestBed.createComponent(SelectOrganisationContaComponent);
        component = fixture.componentInstance;
    });
  
    it('should create', () => {
        expect(component).toBeTruthy();
    });
 
    it('should be named `SelectOrganisationContaComponent`',() => {
        expect(SelectOrganisationContaComponent.name).toBe('SelectOrganisationContaComponent');
    });

    it('should have a method called `constructor`', () => {
        expect(SelectOrganisationContaComponent.prototype.constructor).toBeDefined();
    });

    it('method `constructor` should not be null', () => {
        expect(SelectOrganisationContaComponent.prototype.constructor).not.toBeNull();
    });

    it('should have a method called `ngOnInit`', () => {
        expect(SelectOrganisationContaComponent.prototype.ngOnInit).toBeDefined();
    });

    it('method `ngOnInit` should not be null', () => {
        expect(SelectOrganisationContaComponent.prototype.ngOnInit).not.toBeNull();
    });

    it('should have a method called `processGrid`', () => {
        expect(SelectOrganisationContaComponent.prototype.processGrid).toBeDefined();
    });

    it('method `processGrid` should not be null', () => {
        expect(SelectOrganisationContaComponent.prototype.processGrid).not.toBeNull();
    });

    it('method `onSubmit` should not be null', () => {
        expect(SelectOrganisationContaComponent.prototype.onSubmit).not.toBeNull();
    });

    it('should have a method called `onSubmit`', () => {
        expect(SelectOrganisationContaComponent.prototype.onSubmit).toBeDefined();
    });
});