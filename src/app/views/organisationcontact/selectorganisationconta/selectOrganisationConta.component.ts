
import { Component, OnInit, ViewChild  } from '@angular/core';
import { TerminalClientService } from '../../../framework/services/terminal-client.service';
import { SELRCDComponent } from '../../../framework/selrcd/selrcd.component';


@Component({
    selector: 'app-selectOrganisationConta',
    templateUrl: './selectOrganisationConta.component.html'
})

export class SelectOrganisationContaComponent implements OnInit {
    @ViewChild(SELRCDComponent)
    selrcd: SELRCDComponent;
    showModal = true;
 
    constructor(private client: TerminalClientService) {
    }

    ngOnInit() {
        this.selrcd.cols = [
			{ field: 'organisation', header: 'Org  ', isReadOnly: true, isSearch: true },
			{ field: 'orgContactSequence', header: 'Seq  ', editable: true, isReadOnly: true, isSearch: true },
			{ field: 'orgContactName', header: 'Name  ', editable: true, isReadOnly: true, isSearch: true },
			{ field: 'orgContactJobTitle', header: 'Job Title  ', editable: true, isReadOnly: true, isSearch: true },
			{ field: 'orgContactStatus', header: 'Sts  ', editable: true, isDropDown: true, isReadOnly: true, isSearch: true },
			{ field: 'orgContactIsDefault', header: 'Dft  ', editable: true, isDropDown: true, isReadOnly: true, isSearch: true },
        ];
        this.selrcd.client=this.client;
    }

    ngDoCheck() {
        this.selrcd.flatVariable = this.client.getModel();
    }
 }
