
import { Component, OnInit, ViewChild  } from '@angular/core';
import { TerminalClientService } from '../../../framework/services/terminal-client.service';
import { SELRCDComponent } from '../../../framework/selrcd/selrcd.component';


@Component({
    selector: 'app-selectOrganisationDump',
    templateUrl: './selectOrganisationDump.component.html'
})

export class SelectOrganisationDumpComponent implements OnInit {
    @ViewChild(SELRCDComponent)
    selrcd: SELRCDComponent;
    showModal = true;
 
    constructor(private client: TerminalClientService) {
    }

    ngOnInit() {
        this.selrcd.cols = [
			{ field: 'organisation', header: 'Org  ', isReadOnly: true, isSearch: true },
			{ field: 'organisationName50a', header: 'Name  ', editable: true, isReadOnly: true, isSearch: true },
        ];
        this.selrcd.client=this.client;
    }

    ngDoCheck() {
        this.selrcd.flatVariable = this.client.getModel();
    }
 }
