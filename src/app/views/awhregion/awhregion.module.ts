
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { SharedModule } from '../../shared.module';
import { Routes, RouterModule } from '@angular/router';
import { SelectSELRCDModule } from '../select.selrcd.module';

import { EditAwhRegionComponent} from './editawhregion/editAwhRegion.component'; 
 
export const ROUTES: Routes = [
    {
        path:'editAwhRegion',
        component: EditAwhRegionComponent
    },
];

@NgModule({
	imports: [
		SharedModule,
		SelectSELRCDModule,
		RouterModule.forChild(ROUTES)
	],

	declarations: [
		EditAwhRegionComponent,
	],

	exports: [
		EditAwhRegionComponent,
	]
})

export class AwhRegionModule {}