import { SharedModule } from '../../../shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule, APP_BASE_HREF } from '@angular/common';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { ComponentFixture } from '@angular/core/testing';
import { TestBed, async } from '@angular/core/testing';
import { TerminalClientService } from '../../../framework/services/terminal-client.service';
import { Logger } from '@nsalaun/ng-logger';
import { GrowlModule } from 'primeng/primeng';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TerminalClientState } from '../../../framework/services/terminal-client.service';
import { BehaviorSubject } from 'rxjs/Rx';
import { EditAwhRegionComponent } from './editAwhRegion.component';

describe('Component: EditAwhRegionComponent', () => {
    let component: EditAwhRegionComponent;
    let fixture: ComponentFixture<EditAwhRegionComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                BrowserModule,
                CommonModule,
                FormsModule,
                HttpClientTestingModule,
                GrowlModule,
                ReactiveFormsModule,
                RouterModule,
                RouterTestingModule,
                SharedModule
            ],
            declarations: [EditAwhRegionComponent],
            providers: [
                Logger,
                {
                    provide: TerminalClientService,
                    useValue: {
                        reply: jasmine.createSpy(null, null),
                        setSignal: jasmine.createSpy(null, null),
                        getModel: jasmine.createSpy(null, null).and.returnValue({
                            //TODO:We have to place respective json data in gdoList
                            gdoList: [{
                                diagnosisDate: '18-07-29',
                                diagnosisTime: '01:24',
                                doctorCode: 1,
                                doctorName: 'Tester',
                                findings1: 'abc',
                                findings2: 'def',
                            }]
                        }),
                        state: new BehaviorSubject(TerminalClientState.INITIALIZING)
                    }
                },
                {
                    provide: APP_BASE_HREF, useValue: '/'
                }
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(EditAwhRegionComponent);
        component = fixture.componentInstance;
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should be named `EditAwhRegionComponent`',() => {
        expect(EditAwhRegionComponent.name).toBe('EditAwhRegionComponent');
    });

    it('should have a method called `ngOnInit`', () => {
        expect(EditAwhRegionComponent.prototype.ngOnInit).toBeDefined();
    });

    it('method `ngOnInit` should not be null', () => {
        expect(EditAwhRegionComponent.prototype.ngOnInit).not.toBeNull();
    });

    it('should have a method called `dialogEvent`', () => {
        expect(EditAwhRegionComponent.prototype.dialogEvent).toBeDefined();
    });

    it('method `dialogEvent` should not be null', () => {
        expect(EditAwhRegionComponent.prototype.dialogEvent).not.toBeNull();
    });

    it('should have a method called `onSubmit`', () => {
        expect(EditAwhRegionComponent.prototype.onSubmit).toBeDefined();
    });

    it('method `onSubmit` should not be null', () => {
        expect(EditAwhRegionComponent.prototype.onSubmit).not.toBeNull();
    });
});