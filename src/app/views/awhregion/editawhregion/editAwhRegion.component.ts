import { FunctionKey } from '../../../framework/function-key-panel/function-key-panel.component';
import { Component, OnInit } from '@angular/core';
import { TerminalClientService } from '../../../framework/services/terminal-client.service';
    
@Component({
    selector: 'app-editAwhRegion',
    templateUrl: './editAwhRegion.component.html'
})
    
export class EditAwhRegionComponent implements OnInit {
    awhRegion: any;
    public dialogData: any;
    public searchParams: FunctionKey[];
    public funcParams: FunctionKey[];
    isModalPopUp: boolean = false;
    screenName:string;
 
    constructor(private client: TerminalClientService) {
        this.funcParams = [
			{ id: 'fKey-03', btnTitle: 'F3=Exit', signal: 'funKey03', display: true ,cmdKey: '03' },
			{ id: 'fKey-06', btnTitle: 'F6=Go', signal: 'funKey06', display: true ,cmdKey: '06' }
		];
		this.searchParams = [
			{ id: 'fKey-04', btnTitle: 'F4=Prompt', signal: 'funKey04', display: true, cmdKey: '04'}
		];
    }
 
	dialogEvent($event) {
         if ($event["col"]) {
             this.searchParams[0]["field"] = "";
             this.searchParams[0]["index"] = "";
             this.dialogData.columns.forEach(obj => {
                 if (obj["openPopModel"] && obj["field"] === $event["col"]) {
                     this.searchParams[0]["field"] = $event["col"];
                     this.searchParams[0]["index"] = $event["index"];
                 }
             });
             return;
         }
         this.awhRegion['_SysSubfile']=$event;
	}
	ngOnInit() {
        this.awhRegion = this.client.getModel();
        this.call();
        this.dialogData = {
            header: 'Edit AWH Region',
            onEscapeOption: true,
            selectionMode: 'multiple',
            isDownload: false,
            selectionOption: false,
            rowsDisplayed: this.awhRegion['_SysSubfilePageSize'],
            
            tablePaginator: true,
            flatVariable: this.awhRegion,
            totalRecords: this.awhRegion['_SysTotalRecords'],
            data: this.awhRegion['_SysSubfile'],
            searchParams: this.searchParams,
            
            columns: [
				{ field: 'awhRegionCode', header: 'Rg  ', isFilter: true, editable: this.awhRegion['_SysProgramMode'] == 'ADD' },
				{ field: 'awhRegionName', header: 'AWH Region Name  ', isFilter: true, editable: true },
				{ field: 'awhRegionOrg', header: 'Org  ', isFilter: true, editable: true },
				{ field: 'awhRegionContact', header: 'Cnt  ', isFilter: true, editable: true },
				{ field: 'contactName', header: 'Contact Name  ', editable: true, isReadOnly: true },
            ]
        };
        if (this.awhRegion['_SysProgramMode'] == 'ADD') {
            this.funcParams[1].btnTitle = 'Change';
            this.dialogData.tablePaginator = false;
        } else if (this.awhRegion['_SysProgramMode'] == 'CHG') {
            this.funcParams[1].btnTitle = 'Add';
        }
    }
    
    process(selected) {
        if (selected === "Delete") {
            this.awhRegion['_SysProgramMode'] = "DEL";
        }
        this.awhRegion["_SysSubfile"].forEach(obj => {
        if (obj["_SysRecordSelected"] === "Y") {
            obj["_SysSelected"] = selected;
            obj["_SysRecordDataChanged"] = "Y";
        }
    });

    this.onSubmit();
    }
    

    onSubmit() {
        this.awhRegion['_SysCmdKey'] = '00';
        this.client.reply();
    }
 
        
    disableAllKeys() {
        this.funcParams.forEach(obj => {
            obj.isDisabled = "disabled";
        });
        this.searchParams.forEach(obj => {
            obj.isDisabled = "disabled";
        });
    
    }
    
    enableAllKeys() {
        this.funcParams.forEach(obj => {
            obj.isDisabled = "";
        });
        this.searchParams.forEach(obj => {
            obj.isDisabled = "";
        });
    
    }
    
    call() {
        this.client.sysConfirmPromptObservable.subscribe(obj => {
            if (obj === "_screenConfirm") {
                this.awhRegion = this.client.getModel();
                this.disableAllKeys();
                this.isModalPopUp = false;
                this.client.confirm("CNF");
            } else if (obj) {
                this.isModalPopUp = true;
                this.screenName=obj;
                this.disableAllKeys();
            } else {
                this.isModalPopUp = false;
                this.screenName="";
                this.enableAllKeys();
            }
        });
    }
    
}
