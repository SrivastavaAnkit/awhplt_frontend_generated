
import { Component, OnInit, ViewChild  } from '@angular/core';
import { TerminalClientService } from '../../../framework/services/terminal-client.service';
import { SELRCDComponent } from '../../../framework/selrcd/selrcd.component';


@Component({
    selector: 'app-selectAwhRegion',
    templateUrl: './selectAwhRegion.component.html'
})

export class SelectAwhRegionComponent implements OnInit {
    @ViewChild(SELRCDComponent)
    selrcd: SELRCDComponent;
    showModal = true;
 
    constructor(private client: TerminalClientService) {
    }

    ngOnInit() {
        this.selrcd.cols = [
			{ field: 'awhRegionCode', header: 'Code  ', isReadOnly: true },
			{ field: 'awhRegionName', header: 'AWH Region Name  ', editable: true, isReadOnly: true },
        ];
        this.selrcd.client=this.client;
    }

    ngDoCheck() {
        this.selrcd.flatVariable = this.client.getModel();
    }
 }
