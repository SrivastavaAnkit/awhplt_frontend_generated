import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule, InputTextModule, ButtonModule, DataTableModule, DialogModule, GrowlModule, AccordionModule, CalendarModule, InputMaskModule, ConfirmDialogModule, FieldsetModule, TooltipModule, DropdownModule } from 'primeng/primeng';
import { SELRCDModule } from '../framework/selrcd/selrcd.module';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TableModule } from 'primeng/table';
import { GridPanelModule } from '../framework/grid-panel/grid-panel.module';
import { HeaderModule } from '../framework/header/header.module';
import { EnterKeyModule } from '../framework/enter-key/enter-key.module';
import { CustomFormModule } from '../framework/custom-form/custom-form.module';
import { FooterModule } from '../framework/footer/footer.module';
import { HttpClientModule } from '@angular/common/http';
import { ErrorsModule } from '../framework/errors/errors.module';
import { Wsut5502SelectCentreComponent } from './centre/wsut5502selectcentre/wsut5502SelectCentre.component';
import { SelectOrganisationDumpComponent } from './organisationdump/selectorganisationdump/selectOrganisationDump.component';
import { SelectOrganisationContaComponent } from './organisationcontact/selectorganisationconta/selectOrganisationConta.component';
import { SelectAwhRegionComponent } from './awhregion/selectawhregion/selectAwhRegion.component';
import { SelEquipmentTypeComponent } from './equipmenttype/selequipmenttype/selEquipmentType.component';


@NgModule({
	imports: [
        RouterModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        InputTextModule,
        ButtonModule,
        BrowserAnimationsModule,
        DataTableModule,
        TableModule,
        GridPanelModule,
        DialogModule,
        GrowlModule,
        GridPanelModule,
        AccordionModule,
        CalendarModule,
        InputMaskModule,
        FieldsetModule,
        ConfirmDialogModule,
        HeaderModule,
        EnterKeyModule,
        CustomFormModule,
        FooterModule,
        TooltipModule,
        HttpClientModule,
        DropdownModule,
        ErrorsModule,
        SELRCDModule

	],

	declarations: [
        Wsut5502SelectCentreComponent,
        SelectOrganisationDumpComponent,
        SelectOrganisationContaComponent,
        SelectAwhRegionComponent,
        SelEquipmentTypeComponent,
        
	],

	exports: [
        Wsut5502SelectCentreComponent,
        SelectOrganisationDumpComponent,
        SelectOrganisationContaComponent,
        SelectAwhRegionComponent,
        SelEquipmentTypeComponent,
        
	]
})

export class SelectSELRCDModule {}