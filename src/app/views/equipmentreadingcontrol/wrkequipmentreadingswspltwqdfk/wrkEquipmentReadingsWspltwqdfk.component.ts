import { Component, OnInit } from '@angular/core';
import { FunctionKey } from '../../../framework/function-key-panel/function-key-panel.component';
import { TerminalClientService } from '../../../framework/services/terminal-client.service';
    
@Component({
    selector: 'app-wrkEquipmentReadingsWspltwqdfk',
    templateUrl: './wrkEquipmentReadingsWspltwqdfk.component.html'
})
    
export class WrkEquipmentReadingsWspltwqdfkComponent implements OnInit {
    equipmentReadingControl: any;
    public dialogData;
    public searchParams: FunctionKey[];
    public funcParams: FunctionKey[];

    equipReadingCtrlSts = [
		{ value: '_C', code: 'C', description: 'C - Created' },
		{ value: '_I', code: 'I', description: 'I - In-Progress' },
		{ value: '_X', code: 'X', description: 'X - Complete' },
	]

    constructor(private client: TerminalClientService) {
        this.funcParams = [
			{ id: 'fKey-03', btnTitle: 'F3=Exit', signal: 'funKey03', display: true ,cmdKey: '03' },
			{ id: 'fKey-12', btnTitle: 'F12=Cancel', signal: 'funKey12', display: true ,cmdKey: '12' },
			{ id: 'fKey-11', btnTitle: 'F11=Show', signal: 'funKey11', display: this.equipmentReadingControl['_SysProgramMode'] != 'ADD' ,cmdKey: '11' }
		];
		this.searchParams = [

		];       
    }

    ngOnInit() {
        this.equipmentReadingControl = this.client.getModel();
        this.dialogData = {
            header: 'Wrk Equipment Readings',
            onEscapeOption: true,
            selectionMode: 'multiple',
            isDownload: false,
            selectionOption: false,            
            rowsDisplayed: this.equipmentReadingControl['_SysSubfilePageSize'],
            dropdownMatchingItem: 'equipReadingCtrlSts',
            tablePaginator: true,
            flatVariable: this.equipmentReadingControl,
            totalRecords: this.equipmentReadingControl['_SysTotalRecords'],
            data: this.equipmentReadingControl['_SysSubfile'],
            searchParams: this.searchParams,
            columns: [
				{ field: 'centreCodeKey', header: 'Ctr  ', isReadOnly: true, isSearch: true },
				{ field: 'centreNameDrv', header: 'Centre Name Drv  ', isReadOnly: true },
				{ field: 'readingRequiredDate', header: 'Req Date  ', isReadOnly: true, isSearch: true },
				{ field: 'equipReadingBatchNo', header: 'Batch No.  ', isReadOnly: true },
				{ field: 'equipReadingCtrlSts', header: 'Sts  ', isReadOnly: true, isSearch: true },
				{ field: 'conditionName', header: '*Condition name ', isReadOnly: true },
            ]
        };
    }

    dialogEvent($event) {
        this.equipmentReadingControl['_SysSubfile'] = $event;
    }
    
    process(selected) {
        if (selected === "Delete") {
            this.equipmentReadingControl['_SysProgramMode'] = "DEL";
        }
        this.equipmentReadingControl["_SysSubfile"].forEach(obj => {
        if (obj["_SysRecordSelected"] === "Y") {
            obj["_SysSelected"] = selected;
            obj["_SysRecordDataChanged"] = "Y";
        }
    });

    this.onSubmit();
    }
    
    onSubmit() {
        this.equipmentReadingControl['_SysCmdKey'] = '00';
        this.client.reply();
    }
}
