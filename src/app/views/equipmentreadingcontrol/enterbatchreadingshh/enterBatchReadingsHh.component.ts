
import { Component, OnInit } from '@angular/core';
import { FunctionKey } from '../../../framework/function-key-panel/function-key-panel.component';
import { TerminalClientService, TerminalClientState } from '../../../framework/services/terminal-client.service';
    
@Component({
    selector: 'app-enterBatchReadingsHh',
    templateUrl: './enterBatchReadingsHh.component.html'
})
    
export class EnterBatchReadingsHhComponent implements OnInit {
    public equipmentReadingControl= {};
    public searchParams: FunctionKey[];
    public funcParams: FunctionKey[];
    isModalPopUp: boolean = false;
    screenName: string;
    dialogData;
 
    constructor(private client: TerminalClientService) {
        this.equipmentReadingControl = client.getModel();
        
    }

    ngOnInit() {
        this.call();
    }
 
    setFocusField(field) {
        this.searchParams[0].field = field;
    }
 
    onSubmit() {
        this.equipmentReadingControl['_SysCmdKey'] = '00';
        this.client.reply();
    }
 
      
    disableAllKeys() {
        this.funcParams.forEach(obj => {
            obj.isDisabled = "disabled";
        });
    
    
    }
    
    enableAllKeys() {
        this.funcParams.forEach(obj => {
            obj.isDisabled = "";
        });
    
    
    }
    
    call() {
        this.client.sysConfirmPromptObservable.subscribe(obj => {
            if (obj === "_screenConfirm") {
                this.equipmentReadingControl = this.client.getModel();
                this.disableAllKeys();
                this.isModalPopUp = false;
                this.client.confirm("CNF");
            } else if (obj) {
                this.isModalPopUp = true;
                this.screenName=obj;
                this.disableAllKeys();
            } else {
                this.isModalPopUp = false;
                this.screenName="";
                this.enableAllKeys();
            }
        });
    }
    
}
