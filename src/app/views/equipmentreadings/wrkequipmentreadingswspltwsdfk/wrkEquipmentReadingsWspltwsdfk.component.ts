import { Component, OnInit } from '@angular/core';
import { FunctionKey } from '../../../framework/function-key-panel/function-key-panel.component';
import { TerminalClientService } from '../../../framework/services/terminal-client.service';
    
@Component({
    selector: 'app-wrkEquipmentReadingsWspltwsdfk',
    templateUrl: './wrkEquipmentReadingsWspltwsdfk.component.html'
})
    
export class WrkEquipmentReadingsWspltwsdfkComponent implements OnInit {
    equipmentReadings: any;
    public dialogData;
    public searchParams: FunctionKey[];
    public funcParams: FunctionKey[];

    

    constructor(private client: TerminalClientService) {
        this.funcParams = [
			{ id: 'fKey-03', btnTitle: 'F3=Exit', signal: 'funKey03', display: true ,cmdKey: '03' },
			{ id: 'fKey-06', btnTitle: 'F6=Add', signal: 'funKey06', display: true ,cmdKey: '06' },
			{ id: 'fKey-12', btnTitle: 'F12=Cancel', signal: 'funKey12', display: true ,cmdKey: '12' }
		];
		this.searchParams = [

		];       
    }

    ngOnInit() {
        this.equipmentReadings = this.client.getModel();
        this.dialogData = {
            header: 'Wrk Equipment Readings',
            onEscapeOption: true,
            selectionMode: 'multiple',
            isDownload: false,
            selectionOption: false,            
            rowsDisplayed: this.equipmentReadings['_SysSubfilePageSize'],
            dropdownMatchingItem: 'readingType',
            tablePaginator: true,
            flatVariable: this.equipmentReadings,
            totalRecords: this.equipmentReadings['_SysTotalRecords'],
            data: this.equipmentReadings['_SysSubfile'],
            searchParams: this.searchParams,
            columns: [
				{ field: 'readingRequiredDate', header: 'Read Date  ', isReadOnly: true },
				{ field: 'readingType', header: 'Tp  ', isReadOnly: true },
				{ field: 'conditionName', header: '*Condition name ', isReadOnly: true },
				{ field: 'readingValue', header: 'Value  ', isReadOnly: true },
				{ field: 'centreCodeKey', header: 'Ct  ', isReadOnly: true },
				{ field: 'awhBuisnessSegment', header: 'Sg  ', isReadOnly: true },
				{ field: 'transferToBuisSeg', header: 'TS  ', isReadOnly: true },
				{ field: 'transferToCentre', header: 'TC  ', isReadOnly: true },
				{ field: 'differenceFromPrevious', header: 'Diff  ', isReadOnly: true },
				{ field: 'idleDaysFromPrevious', header: 'Idle  ', isReadOnly: true },
				{ field: 'unavailableDaysFromPrv', header: 'UnAvail  ', isReadOnly: true },
				{ field: 'readingComment', header: 'Reading Comment  ', isReadOnly: true },
            ]
        };
    }

    dialogEvent($event) {
        this.equipmentReadings['_SysSubfile'] = $event;
    }
    
    process(selected) {
        if (selected === "Delete") {
            this.equipmentReadings['_SysProgramMode'] = "DEL";
        }
        this.equipmentReadings["_SysSubfile"].forEach(obj => {
        if (obj["_SysRecordSelected"] === "Y") {
            obj["_SysSelected"] = selected;
            obj["_SysRecordDataChanged"] = "Y";
        }
    });

    this.onSubmit();
    }
    
    onSubmit() {
        this.equipmentReadings['_SysCmdKey'] = '00';
        this.client.reply();
    }
}
