
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { SharedModule } from '../../shared.module';
import { Routes, RouterModule } from '@angular/router';
import { SelectSELRCDModule } from '../select.selrcd.module';

import { EdtEquipmentTypesComponent} from './edtequipmenttypes/edtEquipmentTypes.component';
import { PmtForEquipmentBkdwnComponent} from './pmtforequipmentbkdwn/pmtForEquipmentBkdwn.component'; 
 
export const ROUTES: Routes = [
    {
        path:'edtEquipmentTypes',
        component: EdtEquipmentTypesComponent
    },
    {
        path:'pmtForEquipmentBkdwn',
        component: PmtForEquipmentBkdwnComponent
    },
];

@NgModule({
	imports: [
		SharedModule,
		SelectSELRCDModule,
		RouterModule.forChild(ROUTES)
	],

	declarations: [
		EdtEquipmentTypesComponent,
		PmtForEquipmentBkdwnComponent,
	],

	exports: [
		EdtEquipmentTypesComponent,
		PmtForEquipmentBkdwnComponent,
	]
})

export class EquipmentTypeModule {}