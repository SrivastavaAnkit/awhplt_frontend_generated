import { SharedModule } from '../../../shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule, APP_BASE_HREF } from '@angular/common';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { ComponentFixture } from '@angular/core/testing';
import { TestBed, async } from '@angular/core/testing';
import { TerminalClientService } from '../../../framework/services/terminal-client.service';
import { Logger } from '@nsalaun/ng-logger';
import { GrowlModule } from 'primeng/primeng';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { SelEquipmentTypeComponent} from './selEquipmentType.component';

describe('Component: SelEquipmentTypeComponent', () => {
    let component: SelEquipmentTypeComponent;
    let fixture: ComponentFixture<SelEquipmentTypeComponent>;
  
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                BrowserModule,
                CommonModule,
                FormsModule,
                GrowlModule,
                HttpClientTestingModule,
                ReactiveFormsModule,
                RouterModule,
                RouterTestingModule,
                SharedModule
            ],
            declarations: [SelEquipmentTypeComponent],
            providers: [
                Logger,
                TerminalClientService,
                {
                    provide: APP_BASE_HREF, useValue: '/'
                }
            ]
        }).compileComponents();
    }));
  
    beforeEach(() => {
        fixture = TestBed.createComponent(SelEquipmentTypeComponent);
        component = fixture.componentInstance;
    });
  
    it('should create', () => {
        expect(component).toBeTruthy();
    });
 
    it('should be named `SelEquipmentTypeComponent`',() => {
        expect(SelEquipmentTypeComponent.name).toBe('SelEquipmentTypeComponent');
    });

    it('should have a method called `constructor`', () => {
        expect(SelEquipmentTypeComponent.prototype.constructor).toBeDefined();
    });

    it('method `constructor` should not be null', () => {
        expect(SelEquipmentTypeComponent.prototype.constructor).not.toBeNull();
    });

    it('should have a method called `ngOnInit`', () => {
        expect(SelEquipmentTypeComponent.prototype.ngOnInit).toBeDefined();
    });

    it('method `ngOnInit` should not be null', () => {
        expect(SelEquipmentTypeComponent.prototype.ngOnInit).not.toBeNull();
    });

    it('should have a method called `processGrid`', () => {
        expect(SelEquipmentTypeComponent.prototype.processGrid).toBeDefined();
    });

    it('method `processGrid` should not be null', () => {
        expect(SelEquipmentTypeComponent.prototype.processGrid).not.toBeNull();
    });

    it('method `onSubmit` should not be null', () => {
        expect(SelEquipmentTypeComponent.prototype.onSubmit).not.toBeNull();
    });

    it('should have a method called `onSubmit`', () => {
        expect(SelEquipmentTypeComponent.prototype.onSubmit).toBeDefined();
    });
});