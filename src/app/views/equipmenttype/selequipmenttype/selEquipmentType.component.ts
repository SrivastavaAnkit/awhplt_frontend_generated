
import { Component, OnInit, ViewChild  } from '@angular/core';
import { TerminalClientService } from '../../../framework/services/terminal-client.service';
import { SELRCDComponent } from '../../../framework/selrcd/selrcd.component';


@Component({
    selector: 'app-selEquipmentType',
    templateUrl: './selEquipmentType.component.html'
})

export class SelEquipmentTypeComponent implements OnInit {
    @ViewChild(SELRCDComponent)
    selrcd: SELRCDComponent;
    showModal = true;
 
    constructor(private client: TerminalClientService) {
    }

    ngOnInit() {
        this.selrcd.cols = [
			{ field: 'equipmentTypeCode', header: 'Type Code ', editable: true, isReadOnly: true, isSearch: true },
			{ field: 'equipmentTypeDesc', header: 'Description  ', editable: true, isReadOnly: true },
        ];
        this.selrcd.client=this.client;
    }

    ngDoCheck() {
        this.selrcd.flatVariable = this.client.getModel();
    }
 }
