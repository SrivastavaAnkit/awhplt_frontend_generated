import { Component, OnInit } from '@angular/core';
import { FunctionKey } from '../../../framework/function-key-panel/function-key-panel.component';
import { TerminalClientService } from '../../../framework/services/terminal-client.service';
    
@Component({
    selector: 'app-pmtForEquipmentBkdwn',
    templateUrl: './pmtForEquipmentBkdwn.component.html'
})
    
export class PmtForEquipmentBkdwnComponent implements OnInit {
    equipmentType: any;
    public dialogData;
    public searchParams: FunctionKey[];
    public funcParams: FunctionKey[];

    printEmailOrDisplay = [
		{ value: '_P', code: 'P', description: 'P - Print' },
		{ value: '_E', code: 'E', description: 'E - Email' },
		{ value: '_D', code: 'D', description: 'D - Display' },
	]

    constructor(private client: TerminalClientService) {
        this.funcParams = [
			{ id: 'fKey-03', btnTitle: 'F3=Exit', signal: 'funKey03', display: true ,cmdKey: '03' }
		];
		this.searchParams = [
			{ id: 'fKey-04', btnTitle: 'F4=Prompt', signal: 'funKey04', display: true, cmdKey: '04'}
		];       
    }

    ngOnInit() {
        this.equipmentType = this.client.getModel();
        this.dialogData = {
            header: 'Pmt for Equipment Bkdwn',
            onEscapeOption: true,
            selectionMode: 'multiple',
            isDownload: false,
            selectionOption: false,            
            rowsDisplayed: this.equipmentType['_SysSubfilePageSize'],
            dropdownMatchingItem: 'selected',
            tablePaginator: true,
            flatVariable: this.equipmentType,
            totalRecords: this.equipmentType['_SysTotalRecords'],
            data: this.equipmentType['_SysSubfile'],
            searchParams: this.searchParams,
            columns: [
				{ field: 'equipmentTypeDesc', header: 'Description  ', isReadOnly: true },
				{ field: 'selected', header: 'Selected  ', isReadOnly: true },
            ]
        };
    }

    dialogEvent($event) {
        this.equipmentType['_SysSubfile'] = $event;
    }
    
    process(selected) {
        if (selected === "Delete") {
            this.equipmentType['_SysProgramMode'] = "DEL";
        }
        this.equipmentType["_SysSubfile"].forEach(obj => {
        if (obj["_SysRecordSelected"] === "Y") {
            obj["_SysSelected"] = selected;
            obj["_SysRecordDataChanged"] = "Y";
        }
    });

    this.onSubmit();
    }
    
    onSubmit() {
        this.equipmentType['_SysCmdKey'] = '00';
        this.client.reply();
    }
}
