import { FunctionKey } from '../../../framework/function-key-panel/function-key-panel.component';
import { Component, OnInit } from '@angular/core';
import { TerminalClientService } from '../../../framework/services/terminal-client.service';
    
@Component({
    selector: 'app-edtEquipmentTypes',
    templateUrl: './edtEquipmentTypes.component.html'
})
    
export class EdtEquipmentTypesComponent implements OnInit {
    equipmentType: any;
    public dialogData: any;
    public searchParams: FunctionKey[];
    public funcParams: FunctionKey[];
    isModalPopUp: boolean = false;
    screenName:string;
 
    constructor(private client: TerminalClientService) {
        this.funcParams = [
			{ id: 'fKey-03', btnTitle: 'F3=Exit', signal: 'funKey03', display: true ,cmdKey: '03' },
			{ id: 'fKey-06', btnTitle: 'F6=Go', signal: 'funKey06', display: true ,cmdKey: '06' },
			{ id: 'fKey-12', btnTitle: 'F12=Cancel', signal: 'funKey12', display: true ,cmdKey: '12' }
		];
		this.searchParams = [
			{ id: 'fKey-04', btnTitle: 'F4=Prompt', signal: 'funKey04', display: true, cmdKey: '04'}
		];
    }
 
	dialogEvent($event) {
         if ($event["col"]) {
             this.searchParams[0]["field"] = "";
             this.searchParams[0]["index"] = "";
             this.dialogData.columns.forEach(obj => {
                 if (obj["openPopModel"] && obj["field"] === $event["col"]) {
                     this.searchParams[0]["field"] = $event["col"];
                     this.searchParams[0]["index"] = $event["index"];
                 }
             });
             return;
         }
         this.equipmentType['_SysSubfile']=$event;
	}
	ngOnInit() {
        this.equipmentType = this.client.getModel();
        this.call();
        this.dialogData = {
            header: 'Edt Equipment Types',
            onEscapeOption: true,
            selectionMode: 'multiple',
            isDownload: false,
            selectionOption: false,
            rowsDisplayed: this.equipmentType['_SysSubfilePageSize'],
            dropdownMatchingItem: 'equipReadingUnit',
            tablePaginator: true,
            flatVariable: this.equipmentType,
            totalRecords: this.equipmentType['_SysTotalRecords'],
            data: this.equipmentType['_SysSubfile'],
            searchParams: this.searchParams,
            dropDownItems: [
                { value: '', code: '', description: '' },
                { value: '_hrs', code: 'hrs', description: 'hrs - Hours'},
                { value: '_km', code: 'km', description: 'km - Kilometres'},
                { value: '_tn', code: 'tn', description: 'tn - Tonnes'},
                { value: '_kg', code: 'kg', description: 'kg - Kilograms'},
                { value: '_kWh', code: 'kWh', description: 'kWh - Kilowatt-hour'},
                { value: '_MJ', code: 'MJ', description: 'MJ - Megajoules'}
			],
            columns: [
				{ field: 'equipmentTypeCode', header: 'Code  ', isFilter: true, editable: true },
				{ field: 'equipmentTypeDesc', header: 'Description  ', isFilter: true, editable: true },
				{ field: 'equipReadingUnit', header: 'Read Unit ', isFilter: true, editable: true, isDropDown: true },
            ]
        };
        if (this.equipmentType['_SysProgramMode'] == 'ADD') {
            this.funcParams[1].btnTitle = 'Change';
            this.dialogData.tablePaginator = false;
        } else if (this.equipmentType['_SysProgramMode'] == 'CHG') {
            this.funcParams[1].btnTitle = 'Add';
        }
    }
    
    process(selected) {
        if (selected === "Delete") {
            this.equipmentType['_SysProgramMode'] = "DEL";
        }
        this.equipmentType["_SysSubfile"].forEach(obj => {
        if (obj["_SysRecordSelected"] === "Y") {
            obj["_SysSelected"] = selected;
            obj["_SysRecordDataChanged"] = "Y";
        }
    });

    this.onSubmit();
    }
    

    onSubmit() {
        this.equipmentType['_SysCmdKey'] = '00';
        this.client.reply();
    }
 
        
    disableAllKeys() {
        this.funcParams.forEach(obj => {
            obj.isDisabled = "disabled";
        });
        this.searchParams.forEach(obj => {
            obj.isDisabled = "disabled";
        });
    
    }
    
    enableAllKeys() {
        this.funcParams.forEach(obj => {
            obj.isDisabled = "";
        });
        this.searchParams.forEach(obj => {
            obj.isDisabled = "";
        });
    
    }
    
    call() {
        this.client.sysConfirmPromptObservable.subscribe(obj => {
            if (obj === "_screenConfirm") {
                this.equipmentType = this.client.getModel();
                this.disableAllKeys();
                this.isModalPopUp = false;
                this.client.confirm("CNF");
            } else if (obj) {
                this.isModalPopUp = true;
                this.screenName=obj;
                this.disableAllKeys();
            } else {
                this.isModalPopUp = false;
                this.screenName="";
                this.enableAllKeys();
            }
        });
    }
    
}
