
import { Component, OnInit } from '@angular/core';
import { FunctionKey } from '../../../framework/function-key-panel/function-key-panel.component';
import { TerminalClientService, TerminalClientState } from '../../../framework/services/terminal-client.service';
    
@Component({
    selector: 'app-addPlantEquipment',
    templateUrl: './addPlantEquipment.component.html'
})
    
export class AddPlantEquipmentComponent implements OnInit {
    public plantEquipment= {};
    public searchParams: FunctionKey[];
    public funcParams: FunctionKey[];
    isModalPopUp: boolean = false;
    screenName: string;
    dialogData;
 
    constructor(private client: TerminalClientService) {
        this.plantEquipment = client.getModel();
        this.funcParams = [
			{ id: 'fKey-03', btnTitle: 'F3=Exit', signal: 'funKey03', display: true ,cmdKey: '03' },
			{ id: 'fKey-12', btnTitle: 'F12=Cancel', signal: 'funKey12', display: true ,cmdKey: '12' }
		];
		this.searchParams = [
			{ id: 'fKey-04', btnTitle: 'F4=Prompt', signal: 'funKey04', display: true, cmdKey: '04'}
		];
    }

    ngOnInit() {
        this.call();
    }
 
    setFocusField(field) {
        this.searchParams[0].field = field;
    }
 
    onSubmit() {
        this.plantEquipment['_SysCmdKey'] = '00';
        this.client.reply();
    }
 
      
    disableAllKeys() {
        this.funcParams.forEach(obj => {
            obj.isDisabled = "disabled";
        });
        this.searchParams.forEach(obj => {
            obj.isDisabled = "disabled";
        });
    
    }
    
    enableAllKeys() {
        this.funcParams.forEach(obj => {
            obj.isDisabled = "";
        });
        this.searchParams.forEach(obj => {
            obj.isDisabled = "";
        });
    
    }
    
    call() {
        this.client.sysConfirmPromptObservable.subscribe(obj => {
            if (obj === "_screenConfirm") {
                this.plantEquipment = this.client.getModel();
                this.disableAllKeys();
                this.isModalPopUp = false;
                this.client.confirm("CNF");
            } else if (obj) {
                this.isModalPopUp = true;
                this.screenName=obj;
                this.disableAllKeys();
            } else {
                this.isModalPopUp = false;
                this.screenName="";
                this.enableAllKeys();
            }
        });
    }
    
}
