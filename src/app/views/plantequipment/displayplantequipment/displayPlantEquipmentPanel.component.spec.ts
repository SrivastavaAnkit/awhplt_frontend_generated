import { SharedModule } from '../../../shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule, APP_BASE_HREF } from '@angular/common';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { ComponentFixture } from '@angular/core/testing';
import { TestBed, async } from '@angular/core/testing';
import { TerminalClientService } from '../../../framework/services/terminal-client.service';
import { Logger } from '@nsalaun/ng-logger';
import { GrowlModule } from 'primeng/primeng';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TerminalClientState } from '../../../framework/services/terminal-client.service';
import { BehaviorSubject } from 'rxjs/Rx';
import { DisplayPlantEquipmentPanelComponent } from './displayPlantEquipmentPanel.component';

describe('Component: DisplayPlantEquipmentPanelComponent', () => {
    let component: DisplayPlantEquipmentPanelComponent;
    let fixture: ComponentFixture<DisplayPlantEquipmentPanelComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                BrowserModule,
                CommonModule,
                FormsModule,
                GrowlModule,
                HttpClientTestingModule,
                ReactiveFormsModule,
                RouterModule,
                RouterTestingModule,
                SharedModule
            ],
            declarations: [DisplayPlantEquipmentPanelComponent],
            providers: [
                Logger,
                {
                    provide: TerminalClientService,
                    useValue: {
                        reply: jasmine.createSpy(null, null),
                        setSignal: jasmine.createSpy(null, null),
                        getModel: jasmine.createSpy(null, null).and.returnValue({
                        }),
                        state: new BehaviorSubject(TerminalClientState.INITIALIZING)
                    }
                },
                {
                    provide: APP_BASE_HREF, useValue: '/'
                }
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DisplayPlantEquipmentPanelComponent);
        component = fixture.componentInstance;
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should be named `DisplayPlantEquipmentPanelComponent`', () => {
        expect(DisplayPlantEquipmentPanelComponent.name).toBe('DisplayPlantEquipmentPanelComponent');
    });

    it('should have a method called `constructor`', () => {
        expect(DisplayPlantEquipmentPanelComponent.prototype.constructor).toBeDefined();
    });

    it('method `constructor` should not be null', () => {
        expect(DisplayPlantEquipmentPanelComponent.prototype.constructor).not.toBeNull();
    });

    it('should have a method called `ngOnInit`', () => {
        expect(DisplayPlantEquipmentPanelComponent.prototype.ngOnInit).toBeDefined();
    });

    it('method `ngOnInit` should not be null', () => {
        expect(DisplayPlantEquipmentPanelComponent.prototype.ngOnInit).not.toBeNull();
    });

    it('should have a method called `onSubmit`', () => {
        expect(DisplayPlantEquipmentPanelComponent.prototype.onSubmit).toBeDefined();
    });

    it('method `onSubmit` should not be null', () => {
        expect(DisplayPlantEquipmentPanelComponent.prototype.onSubmit).not.toBeNull();
    });
});