import { Component, OnInit } from '@angular/core';
import { FunctionKey } from '../../../framework/function-key-panel/function-key-panel.component';
import { TerminalClientService } from '../../../framework/services/terminal-client.service';
    
@Component({
    selector: 'app-wrkPlantEquipment',
    templateUrl: './wrkPlantEquipment.component.html'
})
    
export class WrkPlantEquipmentComponent implements OnInit {
    plantEquipment: any;
    public dialogData;
    public searchParams: FunctionKey[];
    public funcParams: FunctionKey[];

    awhBuisnessSegment = [
		{ value: '_01', code: '01', description: '01 - Wool Handling' },
		{ value: '_02', code: '02', description: '02 - Dumping' },
		{ value: '_03', code: '03', description: '03 - Cotton' },
		{ value: '_04', code: '04', description: '04 - Logistics' },
		{ value: '_05', code: '05', description: '05 - Property' },
		{ value: '_06', code: '06', description: '06 - Dry Bulk' },
		{ value: '_09', code: '09', description: '09 - Re-Handle' },
	]
readingType = [
		{ value: '_ST', code: 'ST', description: 'ST - Standard Reading' },
		{ value: '_SV', code: 'SV', description: 'SV - Service Reading' },
		{ value: '_TO', code: 'TO', description: 'TO - Transfer Out' },
		{ value: '_TI', code: 'TI', description: 'TI - Transfer In Reading' },
		{ value: '_IL', code: 'IL', description: 'IL - Initial reading' },
		{ value: '_RT', code: 'RT', description: 'RT - Retirement' },
		{ value: '_NE', code: 'NE', description: 'NE - Any' },
		{ value: '_MR', code: 'MR', description: 'MR - Meter Reset' },
		{ value: '_UA', code: 'UA', description: 'UA - Make UnAvailable' },
		{ value: '_AV', code: 'AV', description: 'AV - Make Available' },
	]

    constructor(private client: TerminalClientService) {
        this.plantEquipment = this.client.getModel();
        this.funcParams = [
			{ id: 'fKey-03', btnTitle: 'F3=Exit', signal: 'funKey03', display: true ,cmdKey: '03' },
			{ id: 'fKey-06', btnTitle: 'F6=Add', signal: 'funKey06', display: true ,cmdKey: '06' },
			{ id: 'fKey-10', btnTitle: 'F10=Visual/Leaser', signal: 'funKey10', display: true ,cmdKey: '10' },
			{ id: 'fKey-11', btnTitle: 'F11=Toggle', signal: 'funKey11', display: this.plantEquipment['_SysProgramMode'] != 'ADD' ,cmdKey: '11' }
		];
		this.searchParams = [

		];       
    }

    ngOnInit() {
        this.dialogData = {
            header: 'Wrk Plant Equipment',
            onEscapeOption: true,
            selectionMode: 'multiple',
            isDownload: false,
            selectionOption: false,            
            rowsDisplayed: this.plantEquipment['_SysSubfilePageSize'],
            dropdownMatchingItem: 'awhBuisnessSegment',
            tablePaginator: true,
            flatVariable: this.plantEquipment,
            totalRecords: this.plantEquipment['_SysTotalRecords'],
            data: this.plantEquipment['_SysSubfile'],
            searchParams: this.searchParams,
            columns: [
				{ field: 'centreCodeKey', header: 'CN  ', isReadOnly: true, isSearch: true },
				{ field: 'awhBuisnessSegment', header: 'Sg  ', isReadOnly: true, isSearch: true },
				{ field: 'equipmentTypeCode', header: 'Type  ', isReadOnly: true, isSearch: true },
				{ field: 'equipmentLeasor', header: 'Leasor  ', isReadOnly: true, isSearch: true },
				{ field: 'plantEquipmentCode', header: 'Code  ', isReadOnly: true, isSearch: true },
				{ field: 'equipmentStatus', header: 'St  ', isReadOnly: true },
				{ field: 'readingFrequency', header: 'F  ', isReadOnly: true },
				{ field: 'equipmentDescription', header: 'Description  ', isReadOnly: true },
				{ field: 'dateDdmm', header: 'Last  ', isReadOnly: true },
				{ field: 'lastReading', header: 'Reading  ', isReadOnly: true },
				{ field: 'readingType', header: 'Typ  ', isReadOnly: true, isSearch: true },
				{ field: 'readingValue', header: 'Service  ', isReadOnly: true },
				{ field: 'visualEquipmentCode', header: 'Vis Code  ', isReadOnly: true },
            ]
        };
    }

    dialogEvent($event) {
        this.plantEquipment['_SysSubfile'] = $event;
    }
    
    process(selected) {
        if (selected === "Delete") {
            this.plantEquipment['_SysProgramMode'] = "DEL";
        }
        this.plantEquipment["_SysSubfile"].forEach(obj => {
        if (obj["_SysRecordSelected"] === "Y") {
            obj["_SysSelected"] = selected;
            obj["_SysRecordDataChanged"] = "Y";
        }
    });

    this.onSubmit();
    }
    
    onSubmit() {
        this.plantEquipment['_SysCmdKey'] = '00';
        this.client.reply();
    }
}
