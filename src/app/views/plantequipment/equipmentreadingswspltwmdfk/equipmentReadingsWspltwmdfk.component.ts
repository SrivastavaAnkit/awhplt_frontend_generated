import { Component, OnInit } from '@angular/core';
import { FunctionKey } from '../../../framework/function-key-panel/function-key-panel.component';
import { TerminalClientService } from '../../../framework/services/terminal-client.service';
    
@Component({
    selector: 'app-equipmentReadingsWspltwmdfk',
    templateUrl: './equipmentReadingsWspltwmdfk.component.html'
})
    
export class EquipmentReadingsWspltwmdfkComponent implements OnInit {
    plantEquipment: any;
    public dialogData;
    public searchParams: FunctionKey[];
    public funcParams: FunctionKey[];

    awhBuisnessSegment = [
		{ value: '_01', code: '01', description: '01 - Wool Handling' },
		{ value: '_02', code: '02', description: '02 - Dumping' },
		{ value: '_03', code: '03', description: '03 - Cotton' },
		{ value: '_04', code: '04', description: '04 - Logistics' },
		{ value: '_05', code: '05', description: '05 - Property' },
		{ value: '_06', code: '06', description: '06 - Dry Bulk' },
		{ value: '_09', code: '09', description: '09 - Re-Handle' },
	]

    constructor(private client: TerminalClientService) {
        this.funcParams = [
			{ id: 'fKey-03', btnTitle: 'F3=Exit', signal: 'funKey03', display: true ,cmdKey: '03' },
			{ id: 'fKey-10', btnTitle: 'F10=Full', signal: 'funKey10', display: true ,cmdKey: '10' }
		];
		this.searchParams = [
			{ id: 'fKey-04', btnTitle: 'F4=Prompt', signal: 'funKey04', display: true, cmdKey: '04'}
		];       
    }

    ngOnInit() {
        this.plantEquipment = this.client.getModel();
        this.dialogData = {
            header: 'Equipment Readings',
            onEscapeOption: true,
            selectionMode: 'multiple',
            isDownload: false,
            selectionOption: false,            
            rowsDisplayed: this.plantEquipment['_SysSubfilePageSize'],
            dropdownMatchingItem: 'awhBuisnessSegment',
            tablePaginator: true,
            flatVariable: this.plantEquipment,
            totalRecords: this.plantEquipment['_SysTotalRecords'],
            data: this.plantEquipment['_SysSubfile'],
            searchParams: this.searchParams,
            columns: [
				{ field: 'awhBuisnessSegment', header: 'Segm  ', isReadOnly: true, isSearch: true },
				{ field: 'equipmentTypeCode', header: 'Type  ', isReadOnly: true, isSearch: true },
				{ field: 'displayPlantEquipCode', header: 'Code  ', isReadOnly: true },
				{ field: 'equipmentDescriptionDrv', header: 'Description  ', isReadOnly: true },
				{ field: 'readingValue', header: 'Reading  ', isFilter: true },
				{ field: 'equipReadingUnit', header: 'Read Unit ', isReadOnly: true },
				{ field: 'previousReadingValue', header: 'Prev.  ', isReadOnly: true },
            ]
        };
    }

    dialogEvent($event) {
        this.plantEquipment['_SysSubfile'] = $event;
    }
    
    process(selected) {
        if (selected === "Delete") {
            this.plantEquipment['_SysProgramMode'] = "DEL";
        }
        this.plantEquipment["_SysSubfile"].forEach(obj => {
        if (obj["_SysRecordSelected"] === "Y") {
            obj["_SysSelected"] = selected;
            obj["_SysRecordDataChanged"] = "Y";
        }
    });

    this.onSubmit();
    }
    
    onSubmit() {
        this.plantEquipment['_SysCmdKey'] = '00';
        this.client.reply();
    }
}
