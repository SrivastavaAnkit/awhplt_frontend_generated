
import { Component, OnInit, ViewChild  } from '@angular/core';
import { TerminalClientService } from '../../../framework/services/terminal-client.service';
import { SELRCDComponent } from '../../../framework/selrcd/selrcd.component';


@Component({
    selector: 'app-wsut5502SelectCentre',
    templateUrl: './wsut5502SelectCentre.component.html'
})

export class Wsut5502SelectCentreComponent implements OnInit {
    @ViewChild(SELRCDComponent)
    selrcd: SELRCDComponent;
    showModal = true;
 
    constructor(private client: TerminalClientService) {
    }

    ngOnInit() {
        this.selrcd.cols = [
			{ field: 'centreCodeKey', header: 'Centre Code  ', editable: true, isReadOnly: true, isSearch: true },
			{ field: 'centreName', header: 'Centre Name  ', editable: true, isReadOnly: true, isSearch: true },
			{ field: 'centreType', header: 'Centre Type  ', editable: true, isDropDown: true, isReadOnly: true },
			{ field: 'stateCode', header: 'State Code  ', editable: true, isDropDown: true, isReadOnly: true },
			{ field: 'centreRegion', header: 'Centre Region  ', editable: true, isReadOnly: true },
        ];
        this.selrcd.client=this.client;
    }

    ngDoCheck() {
        this.selrcd.flatVariable = this.client.getModel();
    }
 }
