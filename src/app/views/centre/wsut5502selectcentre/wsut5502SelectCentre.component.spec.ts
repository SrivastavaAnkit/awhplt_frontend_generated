import { SharedModule } from '../../../shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule, APP_BASE_HREF } from '@angular/common';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { ComponentFixture } from '@angular/core/testing';
import { TestBed, async } from '@angular/core/testing';
import { TerminalClientService } from '../../../framework/services/terminal-client.service';
import { Logger } from '@nsalaun/ng-logger';
import { GrowlModule } from 'primeng/primeng';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Wsut5502SelectCentreComponent} from './wsut5502SelectCentre.component';

describe('Component: Wsut5502SelectCentreComponent', () => {
    let component: Wsut5502SelectCentreComponent;
    let fixture: ComponentFixture<Wsut5502SelectCentreComponent>;
  
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                BrowserModule,
                CommonModule,
                FormsModule,
                GrowlModule,
                HttpClientTestingModule,
                ReactiveFormsModule,
                RouterModule,
                RouterTestingModule,
                SharedModule
            ],
            declarations: [Wsut5502SelectCentreComponent],
            providers: [
                Logger,
                TerminalClientService,
                {
                    provide: APP_BASE_HREF, useValue: '/'
                }
            ]
        }).compileComponents();
    }));
  
    beforeEach(() => {
        fixture = TestBed.createComponent(Wsut5502SelectCentreComponent);
        component = fixture.componentInstance;
    });
  
    it('should create', () => {
        expect(component).toBeTruthy();
    });
 
    it('should be named `Wsut5502SelectCentreComponent`',() => {
        expect(Wsut5502SelectCentreComponent.name).toBe('Wsut5502SelectCentreComponent');
    });

    it('should have a method called `constructor`', () => {
        expect(Wsut5502SelectCentreComponent.prototype.constructor).toBeDefined();
    });

    it('method `constructor` should not be null', () => {
        expect(Wsut5502SelectCentreComponent.prototype.constructor).not.toBeNull();
    });

    it('should have a method called `ngOnInit`', () => {
        expect(Wsut5502SelectCentreComponent.prototype.ngOnInit).toBeDefined();
    });

    it('method `ngOnInit` should not be null', () => {
        expect(Wsut5502SelectCentreComponent.prototype.ngOnInit).not.toBeNull();
    });

    it('should have a method called `processGrid`', () => {
        expect(Wsut5502SelectCentreComponent.prototype.processGrid).toBeDefined();
    });

    it('method `processGrid` should not be null', () => {
        expect(Wsut5502SelectCentreComponent.prototype.processGrid).not.toBeNull();
    });

    it('method `onSubmit` should not be null', () => {
        expect(Wsut5502SelectCentreComponent.prototype.onSubmit).not.toBeNull();
    });

    it('should have a method called `onSubmit`', () => {
        expect(Wsut5502SelectCentreComponent.prototype.onSubmit).toBeDefined();
    });
});