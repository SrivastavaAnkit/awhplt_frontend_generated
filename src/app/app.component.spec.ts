import { SharedModule } from './shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { CommonModule, APP_BASE_HREF } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { AuthService } from './framework/services/auth.service';
import { MessagesService } from './framework/services/messages.service';
import { AppService } from './app.service';
import { TerminalClientService } from './framework/services/terminal-client.service';
import { RouterModule, Router } from '@angular/router';
import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { AppComponent } from './app.component';

describe('Component: AppComponent', () => {
    let component: AppComponent;
    let fixture: ComponentFixture<AppComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                BrowserModule,
                RouterModule,
                CommonModule,
                FormsModule,
                ReactiveFormsModule,
                SharedModule
            ],
            declarations: [AppComponent],
            providers: [AppService, { provide: APP_BASE_HREF, useValue: '/' }]
        }).compileComponents();
  }));

  beforeEach(() => {
      fixture = TestBed.createComponent(AppComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
  });

  it('should create', () => {
      expect(component).toBeTruthy();
  });

  it('should be named `AppComponent`', () => {
      expect(AppComponent.name).toBe('AppComponent');
  });

  it('should have a method called `constructor`', () => {
      expect(AppComponent.prototype.constructor).toBeDefined();
  });

  it('method `constructor` should not be null', () => {
      expect(AppComponent.prototype.constructor).not.toBeNull();
  });

  it('should have a method called `ngOnit`', () => {
      expect(AppComponent.prototype.ngOnInit).toBeDefined();
  });

  it('should have a method called `ngOnit`', () => {
      expect(AppComponent.prototype.ngOnInit).toBeDefined();
  });

  it('should have a method called `navigationInterceptor`', () => {
      expect(AppComponent.prototype.navigationInterceptor).toBeDefined();
  });

  it('method `navigationInterceptor` should not be null', () => {
      expect(AppComponent.prototype.navigationInterceptor).not.toBeNull();
  });

  it('should have a method called `showLoader`', () => {
      expect(AppComponent.prototype.showLoader).toBeDefined();
  });

  it('method `showLoader` should not be null', () => {
      expect(AppComponent.prototype.showLoader).not.toBeNull();
  });
});
