import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ErrorsRoutesModule } from './errors-routes.module';
import { HeaderModule } from '../header/header.module';
import { FooterModule } from '../footer/footer.module';
import {ButtonModule} from 'primeng/primeng';

@NgModule({
  imports: [
    CommonModule,
    ErrorsRoutesModule,
    HeaderModule,
    FooterModule,
    ButtonModule,

  ],
  declarations: [
    PageNotFoundComponent,
  ]
})
export class ErrorsModule { }
