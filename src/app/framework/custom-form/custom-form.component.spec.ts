import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomFormComponent } from './custom-form.component';
import { FormBuilder, ReactiveFormsModule, FormsModule, FormGroup } from '@angular/forms';
import { RmiFieldComponent } from './awh-field/awh-field.component';

describe('CustomFormComponent', () => {
    let component: CustomFormComponent;
    let fixture: ComponentFixture<CustomFormComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports : [FormsModule, ReactiveFormsModule],
            declarations: [ CustomFormComponent, RmiFieldComponent],

        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(CustomFormComponent);
        component = fixture.componentInstance;
        component.formContents =
        [{ id: 'fake_label', y: '1', x: '61', width: '4', for: 'fake', label: 'fake', type: 'label', class: '' }];
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

});
