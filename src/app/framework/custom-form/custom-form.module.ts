import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RmiFieldComponent } from './awh-field/awh-field.component';
import { CustomFormComponent } from './custom-form.component';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule
    ],
    declarations: [
        RmiFieldComponent,
        CustomFormComponent,
    ],
    exports: [
        CustomFormComponent
    ]
})

export class CustomFormModule { }
