import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RmiFieldComponent } from './awh-field.component';
import { ReactiveFormsModule, FormsModule, FormGroup } from '@angular/forms';

describe('RmiFieldComponent', () => {
  let component: RmiFieldComponent;
  let fixture: ComponentFixture<RmiFieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports : [FormsModule, ReactiveFormsModule],
      declarations: [ RmiFieldComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RmiFieldComponent);
    component = fixture.componentInstance;
    component.formGroup = new FormGroup({});
    component.fieldParam = { id: 'fake_label', y: '1', x: '61', width: '4', for: 'fake', label: 'fake', type: 'label', class: '' };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
