import { Injectable, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

import { Message } from 'primeng/primeng';

import { AppConfiguration } from '../utils/app-configuration';
import { AppService } from '../../app.service';
import { GlobalVariable } from './global-variables';
import { StateManager } from '../utils/state-manager';
import { UiUtils } from '../utils/ui-utils';

@Injectable()
export class Auth {
    errorMsg: Message[] = [];

    constructor(
        private router: Router,
        private globalVariable: GlobalVariable,
        private state: StateManager,
        private appConfig: AppConfiguration) {
    }

    processUserDetails(reponse: any) {
        this.globalVariable.$authenticated = reponse.principal.authenticated;
        this.globalVariable.$previousMenu = this.router.url.substr(1);
        this.globalVariable.$activeMenu = 'App.'.concat(reponse.nextStep.program.split('.').pop().replace('Controller', ''));
        this.globalVariable.$companyNumber = reponse.changeDistrictMenuDTO.companyNumber;
        this.globalVariable.$districtNumber = reponse.changeDistrictMenuDTO.districtNumber;
        this.globalVariable.$token = reponse.principal.token;
        this.globalVariable.$user = reponse.principal.name;
        this.globalVariable.$userDetails = reponse.principal;
        this.globalVariable.$flatVariable = reponse.changeDistrictMenuDTO;

        // Locale specific
        const locale = localStorage.getItem('locale');

        if (locale && locale != null) {
            this.globalVariable.$locale = locale;
        } else {
            this.globalVariable.$locale = this.appConfig.get('locale');
            localStorage.setItem('locale', this.appConfig.get('locale'));
        }
    }

    checkStatus(transitionToState: any) {
        let status: boolean;

        if (transitionToState.url !== '' && transitionToState.url !== 'App.Login') {
            this.state.getState(transitionToState.name);
            if (!this.globalVariable.$authenticated) {
                this.errorMsg = [];
                this.errorMsg.push({ severity : 'warning', summary : 'Warning', detail : 'User is not authenticated' });
                status = false;
            } else {
                status = true;
            }
        }

        return status;
    }
}
