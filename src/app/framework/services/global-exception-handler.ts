import { Injectable, ErrorHandler, ReflectiveInjector, ViewContainerRef } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { AppDataService } from '../../app.data.service';
import { MessagesService } from './messages.service';

export enum StatusCode {
    SUCCESS = 200,
    FATAL_ERROR = 500,
    BAD_REQUEST = 400,
    UNAUTHORIZED = 401,
    NOTACCEPTABLE = 406,
    NOT_FOUND = 404
}

@Injectable()
export class GlobalExceptionHandler implements ErrorHandler {
    private showError = false;
    observable: Observable<any>;

    constructor(public msgService: MessagesService) {
    }

    handleError(error) {
    }

    handleHttpErrorResponse(error: HttpErrorResponse) {
        if (error instanceof HttpErrorResponse) {
            if (error.status === StatusCode.UNAUTHORIZED) {
                this.msgService.pushToMessages('error', 'Error', error.statusText);
            } else if (error.status === StatusCode.NOTACCEPTABLE) {
                this.msgService.pushToMessages('error', 'Error', error.statusText);
            } else if (error.status === StatusCode.BAD_REQUEST) {
                this.msgService.pushToMessages('error', 'Error', error.statusText);
            } else if (error.status === StatusCode.NOT_FOUND) {
                this.msgService.pushToMessages('error', 'Error', error.statusText);
            } else if (error.status === StatusCode.FATAL_ERROR) {
                this.msgService.pushToMessages('error', 'Error', error.statusText);
            } else if (error.statusText === 'Unknown Error') {
                this.msgService.pushToMessages('error', 'Error', 'Either Server is stopped or not responding');
            } else {
                this.msgService.pushToMessages('error', 'Error', error.statusText);
            }
        }
    }

    /**
    * Method to handle the Field Errors of Html page.
    * @param errorMessage
    * @param fieldErrors
    */
    handleFieldError(errorMessage: string, fieldErrors) {
        if ((errorMessage.length !== 0) && (errorMessage !== 'Field validation error')) {
            this.showError = true;
            fieldErrors.validation('logic', false);
        }

        if (fieldErrors === null) {
            return;
        }

        Object.keys(fieldErrors).forEach(function (field) {
            const fldError = fieldErrors[field];
            field = field.replace(/.*\./gi, '');
            this.fieldErrors[field].validation('logic', false);
            this.fieldErrors[field].errorMessage = fldError;
        }.bind(this));
    }
}

