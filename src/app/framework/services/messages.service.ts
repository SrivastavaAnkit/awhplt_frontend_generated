import { Injectable } from '@angular/core';
import { Message } from 'primeng/components/common/api';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class MessagesService {
    growlMessages: Message[] = [];
    growlMessageObservable: BehaviorSubject<Message[]> = new BehaviorSubject<Message[]>([]);
    notificationSeverity: string;
    notificationMessage: string;    //errMsgs = require('./messages-en.json');
    message: string;

    constructor() {
    }

    pushToMessages(MessageSeverity: string, MessageSummary: string, MessageDetail: string): void {
        this.growlMessages = [];
        this.growlMessages.push({ 'severity': MessageSeverity, 'summary': MessageSummary, 'detail': MessageDetail });
        this.growlMessageObservable.next(this.growlMessages);
    }

    pushToMessage(MessageDetail: Object): void {
        this.growlMessages = [];
        Object.keys(MessageDetail).forEach(errMessage => {
            if (errMessage == 'SNDERRMSG') {
                this.notificationSeverity = 'error';
            } else if (errMessage == 'SNDSTSMSG') {
                this.notificationSeverity = 'info';
            } else if (errMessage == 'SNDINFMSG') {
                this.notificationSeverity = 'info';
            } else if (errMessage == 'SNDCMPMSG') {
                this.notificationSeverity = 'success';
            }
            this.notificationMessage = MessageDetail[errMessage];
            this.growlMessages.push({ 'severity': this.notificationSeverity, 'summary': '', 'detail': this.notificationMessage });
        });
        this.growlMessageObservable.next(this.growlMessages);
    }

    clearMessages() {
        this.growlMessages = [];
        this.growlMessageObservable.next(this.growlMessages);
    }

    lookupAndShow(code: string): void {
        //this.message = this.errMsgs[code.toLowerCase()];
        if (this.message !== undefined && this.message !== '' && this.message !== null ) {
            this.pushToMessages('error', 'Error', this.message);
        }
    }
}
