import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';

import { GlobalVariable } from './../../framework/services/global-variables';
import { RefreshService } from './refresh-service';

@Injectable()
export class RefreshResolver implements Resolve<any> {

    constructor(private refreshService: RefreshService, private globalVariable: GlobalVariable) {
    }

    resolve(route: ActivatedRouteSnapshot) {
        if (this.globalVariable.$flatVariable === undefined || this.globalVariable.$flatVariable == null) {
            return this.refreshService.getPgmData();
        }
    }
}
