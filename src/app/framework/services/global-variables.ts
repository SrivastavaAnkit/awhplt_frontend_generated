import { Injectable } from '@angular/core';

Injectable();
export class GlobalVariable {
    private companyNumber: any;
    private districtNumber: any;
    private user: any;
    private userDetails: any;
    private token: any;
    private authenticated: any;
    private previousMenu: any;
    private activeMenu: any;
    private flatVariable: any;
    private templateName: string;
    private dynamicModule: any;
    private wsUrl: string;
    private display: string;
    private locale: string;
    private nextStep: any;

    public get $display(): any {
        return this.display;
    }

    public set $display(value: any) {
        this.display = value;
    }

    public get $nextStep(): any {
        return this.nextStep;
    }

    public set $nextStep(value: any) {
        this.nextStep = value;
    }

    public get $companyNumber(): any {
        return this.companyNumber;
    }

    public set $companyNumber(value: any) {
        this.companyNumber = value;
    }

    public get $districtNumber(): any {
        return this.districtNumber;
    }

    public set $districtNumber(value: any) {
        this.districtNumber = value;
    }

    public get $user(): any {
        return this.user;
    }

    public set $user(value: any) {
        this.user = value;
    }

    public get $userDetails(): any {
        return this.userDetails;
    }

    public set $userDetails(value: any) {
        this.userDetails = value;
    }

    public get $token(): any {
        return this.token;
    }

    public set $token(value: any) {
        this.token = value;
    }

    public get $authenticated(): any {
        return this.authenticated;
    }

    public set $authenticated(value: any) {
        this.authenticated = value;
    }

    public get $previousMenu(): any {
        return this.previousMenu;
    }

    public set $previousMenu(value: any) {
        this.previousMenu = value;
    }

    public get $flatVariable(): any {
        return this.flatVariable;
    }

    public set $flatVariable(value: any) {
        this.flatVariable = value;
    }

    public get $activeMenu(): any {
        return this.activeMenu;
    }

    public set $activeMenu(value: any) {
        this.activeMenu = value;
    }

    public get $dynamicModule(): any {
        return this.dynamicModule;
    }

    public set $dynamicModule(value: any) {
        this.dynamicModule = value;
    }

    public get $templateName(): any {
        return this.templateName;
    }

    public set $templateName(value: any) {
        this.templateName = value;
    }

    public get $wsUrl(): any {
        return this.wsUrl;
    }

    public set $wsUrl(value: any) {
        this.wsUrl = value;
    }

    public get $locale(): string {
        return this.locale;
    }

    public set $locale(value: string) {
        this.locale = value;
    }

    public clear() {
        this.$authenticated = undefined;
        this.$previousMenu = null;
        this.$activeMenu = null;
        this.$companyNumber = undefined;
        this.$districtNumber = undefined;
        this.$token = null;
        this.$user = null;
        this.$userDetails = null;
        this.$flatVariable = null;
        this.$locale = null;
    }
}
