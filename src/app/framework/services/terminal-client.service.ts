import { ConfirmationService } from "primeng/primeng";
import { Injectable } from "@angular/core";
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Router } from "@angular/router";
import { Subscription } from "rxjs/Subscription";
import { MessagesService } from "./messages.service";
import { Logger } from "@nsalaun/ng-logger";
import { AuthService } from "./auth.service";
import * as URI from "urijs";
import { Subject } from "rxjs/Subject";

export const TerminalClientState = {
    INITIALIZING: "INITIALIZING",
    NOT_AUTHENTICATED: "NOT_AUTHENTICATED",
    AUTHENTICATING: "AUTHENTICATING",
    NOT_CONNECTED: "NOT_CONNECTED",
    WAITING_REQUEST: "WAITING_REQUEST",
    SENDING_REPLY: "SENDING_REPLY",
    STALLED: "STALLED",
    ON_SCREEN: "ON_SCREEN"
};

@Injectable()
export class TerminalClientService {
    state: BehaviorSubject<string> = new BehaviorSubject<string>(
        TerminalClientState.INITIALIZING
    );
    connected: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    session: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    private reqSub: BehaviorSubject<Subscription> = new BehaviorSubject<
        Subscription
    >(null);
    private replySub: BehaviorSubject<Subscription> = new BehaviorSubject<
        Subscription
    >(null);
    sysConfirmPromptObservable: Subject<any> = new Subject();
    private currentRequest = null;
    private replied = false;
    private headers = new HttpHeaders({ "Content-Type": "application/json" });
  	private _SysNextScreen: any = "";
    // FIXME: Please put into parameters
    private baseURL = URI("http://localhost:8082");

    constructor(
        private http: HttpClient,
        private router: Router,
        private msgService: MessagesService,
        private authService: AuthService,
        private confirmationService: ConfirmationService
    ) {
          this.authService.authenticated.distinctUntilChanged().subscribe(value => {
            if (value) {
              this.restoreSession();
            } else {
              // TODO handle that we're destroyed ... :(
            }
      });

    // Calculate connected
    Observable.combineLatest(this.authService.authenticated, this.session)
        .map(value => Boolean(value[0] && value[1]))
        .subscribe(this.connected);

    const that = this;
    // Calculate state
    Observable.combineLatest(
        this.authService.authenticated,
        this.session,
        this.reqSub,
        this.replySub
    )
      .map(v => {
        const authenticated = v[0];
        const session = v[1];
        const reqSub = v[2];
        const replySub = v[3];

        if (!Boolean(v[0])) {
          return TerminalClientState.NOT_AUTHENTICATED;
        } else if (!Boolean(v[1])) {
          return TerminalClientState.NOT_CONNECTED;
        } else if (Boolean(v[2])) {
          return TerminalClientState.WAITING_REQUEST;
        } else if (Boolean(v[3])) {
          return TerminalClientState.SENDING_REPLY;
        } else {
          if (this.currentRequest && !this.replied) {
            return TerminalClientState.ON_SCREEN;
          } else {
            return TerminalClientState.STALLED;
          }
        }
      })
      .distinctUntilChanged()
      .do(v => console.log("Client state:" + v))
      .subscribe(this.state);

    this.state.subscribe(v => {
      if (v === TerminalClientState.NOT_AUTHENTICATED) {
        this.router.navigate(["/"]);
      } else if (!this.currentRequest) {
        this.router.navigate(["/wait"]);
      }
      if (v === TerminalClientState.STALLED) {
        this.executeNextRequest();
      }
    });
  }

  private setSession(terminalSession) {
    if (terminalSession) {
      sessionStorage["terminalSession"] = JSON.stringify(terminalSession);
    }
    this.session.next(terminalSession);
  }

  private restoreSession() {
    const terminalSession = JSON.parse(
      sessionStorage.getItem("terminalSession")
    );
    if (terminalSession) {
      // "/session/{" + PARAM_TSID + "}"
      console.log("Validating existing session.");
      this.http
        .get<any>(
          URI(this.baseURL)
            .segment("/clientapi/v1/session/")
            .segment(terminalSession.requestId)
            .toString()
        )
        .subscribe(
          v => {
            if (v.requestId === terminalSession.requestId) {
              console.log("Session restored successfully.");
              this.setSession(v);
            } else {
              console.log("Session is invalid.");
              this.setSession(null);
              this.requestNewSession();
            }
          },
          err => {
            console.log("Error while validating session.");
            console.log(err.message);
            this.setSession(null);
          }
        );
    } else {
      console.log("No session to restore.");
      this.setSession(null);
      this.requestNewSession();
    }
  }

  private requestNewSession() {
    this.http
        .post(URI(this.baseURL)
            .segment("/clientapi/v1/session")
            .toString(),
            {}
        )
        .subscribe(session => {
            console.log("request new session:", JSON.stringify(session));
            this.setSession(session);
        });
  }

  public getModel() {
      return this.currentRequest ? this.currentRequest.model : null;
  }

  public setSignal(signal?: string, value?: any, cmdKey?: string, field?: string, index?: string) {
      if (signal) {
          if (typeof value !== "undefined") {
              if (cmdKey === "") {
                  this.currentRequest.model["_SysCmdKey"] = "00";
              } else {
                  this.currentRequest.model["_SysCmdKey"] = cmdKey;
              }
              this.currentRequest.model["_SysEntrySelected"] = field;
//            this.currentRequest.model[signal] = value;
          } else {
//        	  this.currentRequest.model[signal] = true;
      }
      if (index) {
      	this.currentRequest.model["_SysSubfile"].forEach((element, idx) => {
          if (idx == index) {
            element._SysRecordSelected = "Y";
          } else {
            element._SysRecordSelected = "N";
          }
        });
      }
    }
  }

  public reply(signal?: string, value?: any) {
	  //this.currentRequest.model["_SysNotifications"] = "";
      if (!this.currentRequest) {
        // TODO Something more routerish.
          this.msgService.pushToMessages(
              "error",
              "Error",
              "Cannot reply since there is no request."
          );
          throw Error("Cannot reply since there is no request.");
      }
      if (this.replied) {
          this.msgService.pushToMessages(
              "error",
              "Error",
              "Cannot reply twice to the same request."
          );
          throw Error("Cannot reply twice to the same request.");
      }
      const tsId = this.session.value.tsId;
      const requestId = this.currentRequest.requestId;
      const model = Object.assign({}, this.currentRequest.model);

//    if (signal) {
//      if (value != null) {
//        model[signal] = value;
//      } else {
//        model[signal] = true;
//      }
//    }

      this.replySub.next(
          this.http
              .post(URI(this.baseURL)
                  .segment("/clientapi/v1/session/")
                  .segment(tsId)
                  .segment("/reply")
                  .toString(),
                  {
                  	  tsId,
        			  requestId,
                      model
                  }
              )
              .subscribe({
                  error: e => {
                      this.msgService.pushToMessages(
                          "error",
                          "Error while communicating with the server",
                          `Message: ${e.message}`
                      );
                      this.replied = false;
                      this.replySub.next(null);
                  },
                  complete: () => {
                      this.replied = true;
                      this.replySub.next(null);
                  }
              })
      );
  }

  private executeNextRequest() {
      const tsId = this.session.getValue().tsId;

      this.reqSub.next(
          this.http
              .get<any>(URI(this.baseURL)
                  .segment("/clientapi/v1/session/")
                  .segment(tsId)
                  .segment("/request")
                  .toString()
              )
              .retry(3) // FIXME Do More constraint on the retry.
              .take(1)
              .subscribe({
                  next: request => {
                      this.currentRequest = Object.assign({}, request);
                      this.replied = false;
                      switch (request.action) {
			              case "SHOW_SCREEN":
			                {
			               	if (request.screenId === "_screenConfirm") {
			               		this.sysConfirmPromptObservable.next("");
                    			this.sysConfirmPromptObservable.next(request.screenId);
                  			} else {                    				
                    				if (request.model["_SysModalScreen"]) {
                    				this.sysConfirmPromptObservable.next("");
                      				this.sysConfirmPromptObservable.next(
                        			request.model["_SysNextScreen"]
                      				);
                      				return;
                    			}
			                    this.router.navigate([""]).then(() => {
			                      this.router.navigate([request.screenId]);
			                      if (request.model["_SysNotifications"]) {
			                        this.msgService.clearMessages();
			                        this.msgService.pushToMessage(
			                          request.model["_SysNotifications"]
			                        );
			                      }
			                    });
			                  }
			                }
			                break;
			              case "CLOSE_SESSION":
			                {
			                  this.router.navigate(["/"]);
			                }
			                break;
            		}
            return request;
          },
          error: e => {
            this.msgService.pushToMessages(
              "error",
              "Error",
              e.error.code + " : " + e.error.message
            );
            this.reqSub.next(null);
          },
          complete: () => {
            this.reqSub.next(null);
          }
        })
    );
  }

  confirm(mode?: string) {
      this.confirmationService.confirm({
        message: "CONFIRM",
        icon: "pi pi-info-circle",
        accept: () => {
          this.sysConfirmPromptObservable.next("");
          this.currentRequest.model["_SysDeferConfirm"] = "Y";
          this.reply();
        },
      	reject: () => {
          this.sysConfirmPromptObservable.next("");
          this.currentRequest.model["_SysDeferConfirm"] = "N";
          this.reply();
      }
      });
  }
}
