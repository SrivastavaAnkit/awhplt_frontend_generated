import { SharedModule } from "./../../shared.module";
import { FunctionKeyPanelComponent } from "./../function-key-panel/function-key-panel.component";
import { EnterKeyModule } from "./../enter-key/enter-key.module";
import { FunctionKeyModule } from "./../functionKey/functionKey.module";
import { FormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule, Routes, RouterLink } from "@angular/router";
import {
  DataTableModule,
  DialogModule,
  ConfirmDialogModule,
  ButtonModule
} from "primeng/primeng";
import { TableModule } from "primeng/table";
import { SELRCDComponent } from "./selrcd.component";

@NgModule({
  imports: [SharedModule],
  declarations: [SELRCDComponent],
  exports: [SELRCDComponent]
})
export class SELRCDModule {}
