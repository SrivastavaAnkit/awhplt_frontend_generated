import { FunctionKey } from "./../function-key-panel/function-key-panel.component";
import { Component, OnInit, ViewChild } from "@angular/core";
import { Table } from "primeng/table";
import { TerminalClientService } from "../services/terminal-client.service";
@Component({
  selector: "app-selrcd",
  templateUrl: "./selrcd.component.html"
})
export class SELRCDComponent implements OnInit {
  public flatVariable: any;
  public funcParams: FunctionKey[];
  isRecordSelected = false;
  cols = [];
  onSort: boolean = false;
  @ViewChild(Table)
  turboTable: Table;
  client: TerminalClientService;

  ngOnInit() {
    this.funcParams = [
      { id: "fKey-03", btnTitle: "F3=Exit", signal: "funKey03", display: true,cmdKey: '03' }
    ];
  }

  processGrid(event) {
    if (this.onSort) {
      this.customSort(event);
      this.turboTable.first =
        this.flatVariable["_SysSubfilePageNumber"] *
        this.flatVariable["_SysSubfilePageSize"];
      this.onSort = false;
      return;
    }
    this.turboTable.first =
      this.flatVariable["_SysSubfilePageNumber"] *
      this.flatVariable["_SysSubfilePageSize"];
  }

  onRowSelect($event) {
    $event.data["_SysRecordDataChanged"] = "Y";
    $event.data["_SysRecordSelected"] = "Y";
  }

  onRowUnselect($event) {
    $event.data["_SysRecordDataChanged"] = "N";
    $event.data["_SysRecordSelected"] = "N";
  }

  navigationLink(cmdKey) {
    this.flatVariable["_SysCmdKey"] = cmdKey;
    this.client.reply();
  }

  customSort(event) {
    this.flatVariable._SysSubfile.sort((t1, t2) => {
      if (event.sortOrder === 1) {
        if (t1[event.sortField] > t2[event.sortField]) {
          return 1;
        }
        if (t1[event.sortField] < t2[event.sortField]) {
          return -1;
        }
      } else {
        if (t1[event.sortField] < t2[event.sortField]) {
          return 1;
        }
        if (t1[event.sortField] > t2[event.sortField]) {
          return -1;
        }
      }
      return 0;
    });
  }

  onSubmit() {
    this.flatVariable["_SysCmdKey"] = "00";
    this.flatVariable._SysSubfile.forEach(element => {
      if (element["_SysRecordDataChanged"] === "Y") {
        this.isRecordSelected = true;
      }
    });
    if (this.isRecordSelected) {
      this.client.reply();
    }
  }
}
