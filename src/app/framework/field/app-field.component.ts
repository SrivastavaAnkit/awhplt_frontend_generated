import { Component, OnInit, Input, Output, OnChanges } from '@angular/core';
import { EventEmitter } from 'events';

@Component({
    selector: 'app-field',
    templateUrl: './app-field.component.html',
    styleUrls: ['./app-field.component.css']
})
export class AppFieldComponent implements OnChanges {

    @Input() type: string;
    @Input() value: any;
    @Input() disabled: boolean;
    @Input() color: string;
    @Input() hideLeadingZeroes: boolean;
    @Input() hideSign: boolean;
    @Input() hideDecimalIfZero: boolean;
    private rightAlign: boolean;
    private reg: RegExp;

    constructor() { }

    ngOnChanges() {
        switch (this.type) {
            case 'uint':
                this.reg = /^[0-9]+$/;
                this.rightAlign = true;
                break;
            case 'snum':
                this.reg = /^[+-\.]?\d+(,\d{3})*(\.\d+)?$/;
                break;
        }
        this.value = this.hideSign ? String(Math.abs(Number(this.value))) : this.value;
        this.value = this.hideLeadingZeroes ? this.value.replace(/^(-?|\+?)0+/, '$1') : this.value;
        this.value = this.hideDecimalIfZero && Number(this.value) % 1 === 0 ? String(Math.floor(Number(this.value))) : this.value;
  }
}
