import { Injectable } from '@angular/core';
import { APP_CONSTS } from './app_consts';

@Injectable()
export class AppConfiguration {
    options = {};

    constructor() {
        this.options = APP_CONSTS;
    }

    get(key: any) {
        if (this.options[key]) {
            return this.options[key];
        } else {
            return null;
        }
    }

    clear() {
        this.options = {};
    }
}
