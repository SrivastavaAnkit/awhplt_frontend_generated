import { StateManager } from './state-manager';
import { Injectable } from '@angular/core';
import { GlobalVariable } from '../services/global-variables';
import { Router } from '@angular/router';

/**
 * Internal Program Router that contructs a Route from the
 * received parameters in the returned object from the backend.
 * Also saves the current Menu State before hitting the actual router.
 */

@Injectable()
export class AppRouter {
    constructor(private stateManager: StateManager,
        private globalVariable: GlobalVariable, private router: Router) {

    }

    route(path, pgmState) {
        if (path == null) {
            return;
        }

        this.stateManager.saveState(path, pgmState);

        if (this.globalVariable.$activeMenu && path === 'MainMenu_menu') {
            this.router.navigateByUrl('App.' + this.globalVariable.$activeMenu);
        } else {
            this.router.navigateByUrl(path);
        }
    }
}
