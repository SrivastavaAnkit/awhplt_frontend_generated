import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector: 'app-wait',
    templateUrl: 'wait.component.html',
    styleUrls: ['wait.component.css']
})

export class WaitComponent implements OnInit {

    show = false;

    private subscription: Subscription;

    constructor() {
    }

    ngOnInit() {}
}
