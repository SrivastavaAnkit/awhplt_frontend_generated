import { DirectiveModule } from "./../directives/directive.module";
import { FormsModule } from "@angular/forms";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import {
    DialogModule,
    ButtonModule,
    ConfirmDialogModule,
    DropdownModule
} from "primeng/primeng";
import { TableModule } from "primeng/table";
import { GridPanelComponent } from "./grid-panel.component";
import { FunctionKeyPanelModule } from "../function-key-panel/function-key-panel.module";

@NgModule({
  imports: [
    CommonModule,
    TableModule,
    DialogModule,
    FormsModule,
    ButtonModule,
    ConfirmDialogModule,
    FunctionKeyPanelModule,
    DirectiveModule,
    DropdownModule
  ],
  declarations: [GridPanelComponent],
  exports: [GridPanelComponent]
})
export class GridPanelModule {}
