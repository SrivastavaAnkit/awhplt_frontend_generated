import { LazyLoadEvent } from 'primeng/components/common/lazyloadevent';
import { FunctionKey } from './../function-key-panel/function-key-panel.component';
import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { Table } from 'primeng/table';
import { TerminalClientService } from '../services/terminal-client.service';
import { MessagesService } from '../services/messages.service';
import { isEmpty } from 'rxjs/operator/isEmpty';

@Component({
  selector: "app-grid-panel",
  templateUrl: "./grid-panel.component.html"
})
export class GridPanelComponent {
  @ViewChild(Table)
  turboTable: Table;
  flatVariable = {};
  selectDropDownItem: any[] = [];
  onSort: boolean = false;

  @Input()
  data: {
    header: string;
    onEscapeOption: boolean; // Handle hide function .
    selectionMode?: string;
    data: any;
    totalRecords: number;
    width?: string;
    selectedIndex?: any;
    height?: string;
    rowsDisplayed?: number;
    tablePaginator?: boolean;
    selectionOption?: boolean;
    isDownload?: boolean;
    editedIndexes?: number[];
    dropDownItems?: any[];
    dropdownMatchingItem?: string;
    columns: TableParam[];
    buttons: PopUpButton[];
    flatVariable?: {};
    searchParams?: FunctionKey[];
  };

  constructor(
    private client: TerminalClientService,
    private message: MessagesService
  ) {}

  @Output()
  dialogEvent = new EventEmitter();

  onClick($event, signal) {
    this.dialogEvent.emit({ $event, signal });
  }

  hide() {
    if (this.data.onEscapeOption) {
      // this.submit();
    }
  }

  getPage(event: LazyLoadEvent) {
    this.flatVariable = this.data.flatVariable;
    if (this.onSort) {
      this.customSort(event);
      this.turboTable.first =
        this.flatVariable["_SysSubfilePageNumber"] *
        this.flatVariable["_SysSubfilePageSize"];
      return;
    }
    this.turboTable.first =
      this.flatVariable["_SysSubfilePageNumber"] *
      this.flatVariable["_SysSubfilePageSize"];
    this.dropDownFieldFilter();
    let notifications = false;
    this.flatVariable["_SysSubfile"].forEach(obj => {
      if (
        obj["_SysNotifications"] &&
        Object.keys(obj["_SysNotifications"]).length !== 0
      ) {
        notifications = true;
      }
    });
    if (notifications) {
      this.message.clearMessages();
      this.message.pushToMessages("error", "Value Required", "Value Required");
    }
    this.dialogEvent.emit(this.data.data);
  }

  dropDownFieldFilter() {
    if (this.data.dropDownItems && this.data.dropdownMatchingItem) {
      this.data.data.forEach((rowData, id) => {
        this.data.dropDownItems.forEach(dropDownItem => {
          if (rowData[this.data.dropdownMatchingItem] === dropDownItem.code) {
            this.selectDropDownItem[id] = dropDownItem;
          }
        });
      }, this);
    }
  }

  customSort(event) {
    this.data.data.sort((t1, t2) => {
      if (event.sortOrder == 1) {
        if (t1[event.sortField] > t2[event.sortField]) {
          this.dropDownFieldFilter();
          return 1;
        }
        if (t1[event.sortField] < t2[event.sortField]) {
          this.dropDownFieldFilter();
          return -1;
        }
      } else {
        if (t1[event.sortField] < t2[event.sortField]) {
          this.dropDownFieldFilter();
          return 1;
        }
        if (t1[event.sortField] > t2[event.sortField]) {
          this.dropDownFieldFilter();
          return -1;
        }
      }
      return 0;
    });
  }

  performFilter(flatVariable) {
    this.data.flatVariable = flatVariable;
    this.getPage(null);
  }

  navigationLink(cmdKey) {
    this.flatVariable["_SysCmdKey"] = cmdKey;
    this.client.reply();
  }

  onRowSelectChange($event, rowIndex) {
    let data = this.data.data[rowIndex];
    if ($event.target.checked) {
      data["_SysSelected"] =
        this.flatVariable["_SysProgramMode"] === "CHG" ? "CHANGE" : "ADD";
      data["_SysRecordSelected"] = "Y";
      this.dialogEvent.emit(this.data.data);
    } else {
      data["_SysSelected"] =
        this.flatVariable["_SysProgramMode"] === "CHG" ? "CHANGE" : "ADD";
      data["_SysRecordSelected"] = "N";
      this.dialogEvent.emit(this.data.data);
    }
  }

  sysRecordChanged(rowIndex) {
    this.data.data[rowIndex]._SysRecordDataChanged = "Y";
    this.dialogEvent.emit(this.data.data);
  }

  setFilterData(filterValue: string, filterColumnName: string) {
    this.flatVariable[filterColumnName] = filterValue;
  }

  changeValue(selected, index, col) {
    this.data.data[index][col] = selected.code;
    this.sysRecordChanged(index);
  }

  openPopModel(index, column) {
    this.dialogEvent.emit({
      openPopModel: true,
      index: index,
      col: column,
      data: this.data.data
    });
  }
}

interface TableParam {
    field: string;
    header: string;
    editable?: boolean;
    style?: string;
    joinColumn: boolean;
    joinColumnsList?: any;
    hideColumn?: boolean;
    isSearch?: boolean;
    isFilter?: boolean;
    isReadOnly?: boolean;
    isDropDown?: boolean;
    openPopModel?: boolean;
    selection?: any;
    totalRecords?: number;
    paginator?: boolean;
    rows?: string;
    rowPerPageOptions?: string;
    resizableColumns?: boolean;
    loading?: boolean;
}

interface PopUpButton {
    label: string;
    icon: string;
}
