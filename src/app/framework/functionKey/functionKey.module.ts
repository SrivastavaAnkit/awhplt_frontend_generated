import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FunctionKeyComponent } from './functionKey.component';
import { RouterModule, Routes, RouterLink } from '@angular/router';

@NgModule({
    imports: [
        CommonModule,
        RouterModule
    ],
    declarations: [
        FunctionKeyComponent,
    ],
    exports: [FunctionKeyComponent]
})

export class FunctionKeyModule { }
