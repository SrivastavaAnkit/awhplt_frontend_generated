import { Directive, HostListener, Renderer, ElementRef } from '@angular/core';

@Directive({
    selector: '[appOnlyLetter]'
})

// <input id="user" type="text" class="input" [(ngModel)]='model.username' onlyLetter>
export class OnlyLetterDirective {
    constructor(private renderer: Renderer, private el: ElementRef) {
    }

    @HostListener('keyup') onKeyUp() {
        const transformedInput = this.el.nativeElement.value.replace(/[^a-zA-Z ]/g, '');

        if (transformedInput !== this.el.nativeElement.text) {
            this.el.nativeElement.value = transformedInput;
        }

        return transformedInput;
    }
}
