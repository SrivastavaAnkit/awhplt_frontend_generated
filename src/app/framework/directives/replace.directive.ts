import { Directive, HostListener, Renderer, ElementRef } from '@angular/core';

@Directive({
    selector: '[appReplace]'
})
export class ReplaceDirective {
    constructor(private renderer: Renderer, private el: ElementRef) {
    }

    @HostListener('keyup') onKeyUp() {
        if (this.el.nativeElement.value.length === 1 && this.el.nativeElement.value === ' ') {
            this.el.nativeElement.value = '0';
        }
    }
}
