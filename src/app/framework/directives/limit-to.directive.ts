import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
    selector: '[appLimitTo]'
})

export class LimitToDirective {
    constructor(private el: ElementRef) {
    }

    @HostListener('keyup') onKeyUp(evt) {
        const limitValue = this.el.nativeElement.attributes.limitTo.value;
        const value = this.el.nativeElement.value;

        if (String(value).length >= limitValue) {
            evt.preventDefault();
        }
    }
}
