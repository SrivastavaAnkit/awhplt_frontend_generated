import { NgModule } from "@angular/core";
import { AutoFocusDirective } from "./auto-focus";
import { CommonModule } from "@angular/common";
import { CapitalizeDirective } from "./capitalize.directive";
import { LimitToDirective } from "./limit-to.directive";
import { OnlyLetterDirective } from "./only-letter.directive";
import { OnlynumberDirective } from "./onlynumber.directive";
import { ProgressIndicatorDirective } from "./progress-indicator.directive";
import { ReplaceDirective } from "./replace.directive";
import { EnableValidationDirective } from "./EnableValidation";
import { BoldDirective } from "./bold.directive";
import { AlertDirective } from "./alert.directive";
import { RightAlignDirective } from "./right-align.directive";

@NgModule({
  imports: [CommonModule],
  declarations: [
    AutoFocusDirective,
    CapitalizeDirective,
    LimitToDirective,
    OnlyLetterDirective,
    OnlynumberDirective,
    ProgressIndicatorDirective,
    ReplaceDirective,
    EnableValidationDirective,
    BoldDirective,
    AlertDirective,
    RightAlignDirective
  ],
  exports: [
    AutoFocusDirective,
    CapitalizeDirective,
    LimitToDirective,
    OnlyLetterDirective,
    OnlynumberDirective,
    ProgressIndicatorDirective,
    ReplaceDirective,
    EnableValidationDirective,
    BoldDirective,
    AlertDirective,
    RightAlignDirective
  ]
})
export class DirectiveModule {}
