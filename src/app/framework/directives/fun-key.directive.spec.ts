import { SignalDirective } from './signal.directive';
import { TestBed, inject } from '@angular/core/testing';
import { Injectable, ElementRef } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { HttpClient, HttpHeaders, HttpHandler } from '@angular/common/http';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { Logger } from '@nsalaun/ng-logger';
import { FunKeyDirective } from './fun-key.directive';

describe('FunKeyDirective', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                {
                    provide: Logger,
                    useValue: {
                        warn: {}
                    }
                },
                {
                    provide: ElementRef,
                    useValue: {
                        nativeElement: {
                            click: {}
                        }
                    }
                },
            ]
        });
    });

    describe('#constructor', () => {
        it('should create an instance', inject([ElementRef, Logger], (el, logger) => {
            const directive = new FunKeyDirective(el, logger);
            expect(directive).toBeDefined();
        }));
    });


    describe('#onKeydown', () => {
        it('should prevent default and click on the button (F1)', inject([ElementRef, Logger], (el, logger) => {
            const directive = new FunKeyDirective(el, logger);
            directive.appFunKey = 'fKey-01';
            const keyboardPressEvent: any = document.createEvent('CustomEvent');
            keyboardPressEvent.which = 112;
            spyOn(el.nativeElement, 'click');
            spyOn(keyboardPressEvent, 'preventDefault');
            keyboardPressEvent.initEvent('keydown', true, true);
            directive.handleKeyboardEvents(keyboardPressEvent);
            expect(keyboardPressEvent.preventDefault).toHaveBeenCalled();
            expect(el.nativeElement.click).toHaveBeenCalled();
        }));

        it('should prevent default and click on the button (Shift + F1)', inject([ElementRef, Logger], (el, logger) => {
            const directive = new FunKeyDirective(el, logger);
            directive.appFunKey = 'fKey-13';
            const keyboardPressEvent: any = document.createEvent('CustomEvent');
            keyboardPressEvent.shiftKey = true;
            keyboardPressEvent.which = 112;
            spyOn(el.nativeElement, 'click');
            spyOn(keyboardPressEvent, 'preventDefault');
            keyboardPressEvent.initEvent('keydown', true, true);
            directive.handleKeyboardEvents(keyboardPressEvent);
            expect(keyboardPressEvent.preventDefault).toHaveBeenCalled();
            expect(el.nativeElement.click).toHaveBeenCalled();
        }));

    });

});
