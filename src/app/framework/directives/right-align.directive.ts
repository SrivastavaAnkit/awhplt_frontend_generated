import { Directive, OnInit, ElementRef } from '@angular/core';

@Directive({
    selector: '[appRightAlign]'
})
export class RightAlignDirective {

    constructor(private el: ElementRef) {
        el.nativeElement.style.textAlign = 'right';
    }

}
