import { Directive, ElementRef, Input, OnInit, DoCheck, Renderer2 } from '@angular/core';
import { NgForm } from '@angular/forms/src/directives';
import { FormControl, FormGroup, Validator, Validators } from '@angular/forms';

/**
 * @author : Vaishno Upadhyay
 * @author : Vivek Kumar Singh
 * @since : 12/15/2017
 *
 * Directive for Validating Field errors in the html form field.
 */

@Directive({
    // tslint:disable-next-line:directive-selector
    selector: '[appEnableValidation]'
})

export class FieldValidationDirective implements OnInit, DoCheck {

    @Input() myform: FormGroup;
    private span: any;
    private length: number;

    constructor(private el: ElementRef, private renderer: Renderer2) {
    }

    ngOnInit() {
        length = this.el.nativeElement.length;
    }

    /**
     * Checking error status on every changes in the html.
    */
    ngDoCheck() {
        let text = '';
        let j;

        Object.keys(this.myform.controls).forEach(key => {
            for (j = 0; j < length; j++) {
                if (key === this.el.nativeElement[j].id) {
                    break;
                }
            }

            if (this.myform.get(key).status === 'INVALID') {
                this.span = this.renderer.createElement('span');

                this.renderer.setAttribute(this.el.nativeElement[j].parentNode, 'id', 'tooltip');
                this.renderer.appendChild(this.el.nativeElement[j].parentNode, this.span);
                this.renderer.setStyle(this.el.nativeElement[j], 'border', '1px solid red');

                if (this.el.nativeElement[j].nextElementSibling != null) {
                    const err = this.myform.get(key).errors;

                    switch (Object.getOwnPropertyNames(this.myform.get(key).errors)[0]) {
                      case 'maxlength':
                          text = 'This field cannot contain more than ' + err.maxlength.requiredLength
                            + ' characters';
                          break;
                      case 'minlength':
                          text = 'This field should contain ' + err.minlength.requiredLength
                            + ' characters';
                          break;
                      case 'required':
                          text = 'This field cannot be blank';
                          break;
                      case 'pattern':
                          text = 'This field should contain numbers';
                      break;
                      case 'number':
                          text = 'This field should contain numbers, cannot have special characters!';
                      break;
                      default: text = 'Unknown error';
                    }

                    this.el.nativeElement[j].nextElementSibling.id = 'tooltiptext';
                    this.el.nativeElement[j].nextElementSibling.innerHTML = text;
                }
            } else {
                this.renderer.setStyle(this.el.nativeElement[j], 'border', '1px solid #cccccc');

                if (this.el.nativeElement[j].nextElementSibling != null) {
                    this.el.nativeElement[j].nextElementSibling.id = '';
                    this.el.nativeElement[j].nextElementSibling.innerHTML = '';
                }
            }
        });
    }
}
