import { Directive, OnInit, ElementRef } from '@angular/core';

@Directive({
    selector: '[appBold]'
})
export class BoldDirective  {

    constructor(private el: ElementRef) {
        el.nativeElement.style.fontWeight = 'bold';
    }

}
