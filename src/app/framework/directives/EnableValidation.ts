import { Directive, ElementRef, Input, OnInit, DoCheck, Renderer2 } from '@angular/core';
import { NgForm } from '@angular/forms/src/directives';
import { FormControl, FormGroup, Validator, Validators } from '@angular/forms';

/**
 * @author : Vaishno Upadhyay
 * @since : 12/15/2017
 *
 * Directive for Validating Field errors in the html form field.
 */

@Directive({
    // tslint:disable-next-line:directive-selector
    selector: '[enablevalidation]'
})

export class EnableValidationDirective implements OnInit, DoCheck {

    @Input() myform: FormGroup;
    private span: any;

    constructor(private el: ElementRef, private renderer: Renderer2) {
    }

    /**
     * Creating span element in every form element in html.
     */
    ngOnInit() {
        const length = this.el.nativeElement.children.length;

        for (let i = 0; i < length; i++) {
            this.span = this.renderer.createElement('span');
            this.renderer.setAttribute(this.el.nativeElement.children[i], 'id', 'tooltip');
            this.renderer.appendChild(this.el.nativeElement.children[i], this.span);
        }
    }

    /**
     * Checking error status on every changes in the html.
     */
    ngDoCheck() {
        let text = '';
        let i = -1;

        Object.keys(this.myform.controls).forEach(key => {
            i++;
            if (this.myform.get(key).status === 'INVALID') {
                this.renderer.setStyle(this.el.nativeElement.attributes[2].ownerElement.elements[i], 'border',
                    '1px solid red');

                if (this.el.nativeElement.children[i].lastChild != null) {
                    const err = this.myform.get(key).errors;

                    switch (Object.getOwnPropertyNames(this.myform.get(key).errors)[0]) {
                        case 'maxlength':
                            text = 'This field cannot contain more than ' + err.maxlength.requiredLength
                                + ' characters';
                            break;
                        case 'minlength':
                            text = 'This field should contain ' + err.minlength.requiredLength
                                + ' characters';
                            break;
                        case 'required':
                            text = 'This field cannot be blank';
                            break;
                        case 'pattern':
                            text = 'This field should contain numbers';
                            break;
                        case 'number':
                            text = 'This field should contain numbers, cannot have special characters!';
                            break;
                        default:
                            text = 'Unknown error';
                    }

                    this.el.nativeElement.children[i].lastChild.id = 'tooltiptext';
                    this.el.nativeElement.children[i].lastChild.innerHTML = text;
                }
            } else {
                this.renderer.setStyle(this.el.nativeElement.attributes[2].ownerElement.elements[i], 'border',
                    '1px solid white');

                if (this.el.nativeElement.children[i].lastChild != null) {
                    this.el.nativeElement.children[i].lastChild.id = '';
                    this.el.nativeElement.children[i].lastChild.innerHTML = '';
                }
            }
        });
    }
}
