import { FunKeyDirective } from "./../directives/fun-key.directive";
import { SignalDirective } from "./../directives/signal.directive";
import { FunctionKeyPanelComponent } from "./function-key-panel.component";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes, RouterLink } from "@angular/router";

@NgModule({
  imports: [CommonModule, RouterModule],
  declarations: [FunctionKeyPanelComponent, SignalDirective, FunKeyDirective],
  exports: [FunctionKeyPanelComponent, SignalDirective, FunKeyDirective]
})
export class FunctionKeyPanelModule {}
