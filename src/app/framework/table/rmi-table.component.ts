import {
    Component, OnInit, ViewEncapsulation, ViewChild, ElementRef,
    AfterViewChecked, Input, Output, EventEmitter, DoCheck, HostListener
} from '@angular/core';
import { Table } from 'primeng/table';
import { MessagesService } from '../services/messages.service';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'app-table',
    templateUrl: './rmi-table.component.html',
    styleUrls: ['./rmi-table.component.css'],
})

export class RMITableComponent implements OnInit {
    @Input() headers: Header[][];
    @Input() records: any;
    @Input() options: Options;
    @Output() onRow = new EventEmitter();
    @ViewChild(Table) turboTable: Table;

    indexArray: number[] = [];
    invalidIndexes = [];


    onRowChange($event, option) {
        switch (option) {
            case 'select':
                this.options.selectedIndex = this.getSelectedIndex();
                break;
            case 'unselect':
                this.options.selectedIndex = -1;
                break;
        }
        if (this.onRow) {
            this.onRow.emit($event);
        }
    }

    indexEditedFields(index: number) {
        this.indexArray.push(this.turboTable.first + index);
        this.options.editedIndexes = Array.from(new Set(this.indexArray));
    }

    getSelectedIndex(): number {
        return this.records.indexOf(this.options.selection);
    }

    constructor(private msgService: MessagesService) {
    }

    ngOnInit() {
    }

    paginate(direction: string) {
        const numberOfPages = Math.ceil(this.turboTable.totalRecords / this.turboTable.rows);
        const indexOfFirstRecordInLastPage = ((this.turboTable.rows * (numberOfPages - 1)));
        if (direction === 'previous' && this.turboTable.first !== 0) {
            this.turboTable.first -= this.turboTable.rows;
        } else if (direction === 'next' && this.turboTable.first !== indexOfFirstRecordInLastPage) {
            this.turboTable.first = Number(this.turboTable.first + this.turboTable.rows);
        }
    }

    hasValidSelections() {
        let unselectedCount = 0;
        this.invalidIndexes = [];
        this.options.selectionValues.push('0');
        for (let i = 0; i < this.options.editedIndexes.length; i++) {
            if (this.options.selectionValues.indexOf(this.turboTable.value[this.options.editedIndexes[i]].slt.toString()) === -1) {
                this.options.selection = null;
                this.invalidIndexes.push(this.options.editedIndexes[i]);
            } else if (this.turboTable.value[this.options.editedIndexes[i]].slt.toString() === '0') {
                unselectedCount++;
            }
        }

        if (unselectedCount === this.options.editedIndexes.length) {
            this.options.selection = null;
            this.msgService.pushToMessages('error', 'Error', 'No Selection Made.');
            return false;
        } else if (this.invalidIndexes.length > 0) {
            this.options.selection = null;
            this.msgService.pushToMessages('error', 'Error', 'Value entered for field is not valid.');
            return false;
        }
        return true;

    }

    @HostListener('document:keydown', ['$event'])
    handleKeyboardEvents(event: KeyboardEvent) {
        const keyCode = event.keyCode || event.which;
        switch (keyCode) {
            case 33:
                event.preventDefault();
                this.paginate('previous');
                break;
            case 34:
                event.preventDefault();
                this.paginate('next');
                break;
        }

    }
}

export interface Header {
    header?: string;
    field?: string;
    rowspan?: number;
    colspan?: number;
    sortable?: boolean;
    editable?: boolean;
    width?: string;
    type: string;
}

export interface Options {
    selectionMode?: string;
    selection?: any;
    selectedIndex?: number;
    paginator?: boolean;
    rows?: string;
    rowPerPageOptions?: string;
    reorderableColumns?: boolean;
    resizableColumns?: boolean;
    loading?: boolean;
    summary?: string;
    editedIndexes?: number[];
    selectionValues?: any[];
    caption?: string;
}
